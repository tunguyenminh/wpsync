<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'syncwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'okRsidUkDiMJt+y0v2RVLtH0vUEpcNOJFIVumYhLx2AepfxMyQs4bDaR+b2BvVfPQC/iQ78kFcUvDLGts5PdVQ==');
define('SECURE_AUTH_KEY',  'ODB18vs/6NRtHYCYLbnSpdXesgD6JNB5+KP4VJncObNcN+YXYfvwpafLG1M1eDqn1h5QQbfUrymimrF8J96qWw==');
define('LOGGED_IN_KEY',    'Drr+MXFyiIiMfrKU95X44I1YVU8pDRUEhdMmIxdaF6+WpIUs2SG+RK6kaBQ9kWs2QihP1wTMj1Zs6L7Y3fg4Sg==');
define('NONCE_KEY',        'Pj91aPjwMgrll2cC2UwoX85dOxMthhi92iJr802zNGtLgQmeLt5JE2cf4oyC1GuvYDbNMe2TL8L+nJXUblq3hg==');
define('AUTH_SALT',        'Nt//Kf846QBHYoXOQqk0AbYoUMEHo622bqoPE9tHX7fsdFoMR+btN1CCcD0eEcORVDJ/CxIWPCCsPA2Vg4ojzw==');
define('SECURE_AUTH_SALT', '7Wjny9QbRSs7XhLHWD67dcMF8SmJdrIKnQQcslv0Mm+QKBn5eGcRJlI8q3hfitW6u+XGxbD+B0cfvAcN3TSmEg==');
define('LOGGED_IN_SALT',   'QgqrqRTdtozJbRk5HUi39xnUik/eGEEyE2fIAV97FCKucCdaw10H0keDD8eRAjNpS7j6R8iIETqJ9TJy+uIBFw==');
define('NONCE_SALT',       'Dp4KpKaWFWuccANKhD6ztgUKIrHIttUp4T2GATuP84M2jnR+6mmTXeuiAf3IU4RCHRS+ptY+VpjLq3D18b4Mmg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
