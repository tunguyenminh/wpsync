<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">
				  <div id="footer-vuanem">
				    <div class="container">
				      <!-- <div class="top-footer" v-html="footertoptext"></div> -->
				      <div class="bottom-footer">
				        <div class="bottom-footer-container">
				          <div class="bottom-footer-item report">
				            <div class="image">
				              <a href="/"><img src="/wp-content/uploads/footer/vuanem-logo.png" alt="vuanem-logo"/></a>
				            </div>
				            <p>Gọi mua hàng (8h30 - 21:00)<br><span class=""><a href="tel:18002092">1800 2092</a></span></p>
				            <p>Gọi khiếu nại (8h00 - 17h30)<br><span class=""><a href="tel:18002093">1800 2093</a></span></p>
				            <p>E-mail<br><span class="">online@vuanem.com</span></p>
				            <p>Nghỉ chiều thứ 7 và Chủ nhật</p>
				          </div>
				          <div class="bottom-footer-item">
				            <p class="title-bottom"><span class="">Thông tin công ty</span></p>
				            <p><a href="/gioi-thieu">Giới thiệu công ty</a></p>
				            <p><a href="/contact">Liên hệ</a></p>
				            <p><a href="/stores" title="Tìm cửa hang (showroom) Vua Nệm gần nhất">Xem hệ thống cửa hàng</a></p>
				            <p><a href="/doi-tac">Đối tác của Vua Nệm</a></p>
				            <p class="social"><span class="">Social</span></p>
				            <div class="social-symbol">
				              <div class="fb-symbol"><a href="https://www.facebook.com/vuanem.vn/" rel="noopener"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
				              <div class="symbol"><a href="https://www.youtube.com/channel/UC6atxoNoxsa2BYKE4_hqwLA" rel="noopener"><i class="fa fa-youtube" aria-hidden="true"></i></a></div>
				              <div class="symbol"><a href="https://www.instagram.com/vuanem/" rel="noopener"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
				            </div>
				          </div>
				          <div class="bottom-footer-item">
				            <p class="title-bottom"><span class="">Tin tức</span></p>
				            <p><a href="/blog/khuyen-mai">Khuyến mại</a></p>
				            <p><a href="/blog/suc-khoe-giac-ngu">Sức Khoẻ Giấc Ngủ</a></p>
				            <p><a href="/blog/chuyen-gia-nem">Chuyên Gia Nệm</a></p>
				            <p><a href="/tuyen-dung">Tuyển dụng</a></p>
				          </div>
				          <div class="bottom-footer-item">
				            <p class="title-bottom"><span class="">Hỗ trợ</span></p>
				            <p><a href="/dieu-khoan-dieu-kien">Điều khoản & Điều kiện</a></p>
				            <p><a href="/chinh-sach-bao-mat">Chính sách bảo mật</a></p>
				            <p><a href="/chinh-sach-bao-hanh">Chính sách bảo hành</a></p>
				            <p><a href="/phuong-thuc-thanh-toan">Phương thức thanh toán</a></p>
				            <p><a href="/chinh-sach-van-chuyen-giao-nhan" title="Miễn phí vận chuyển giao nhận nệm">Chính sách vận chuyển & giao nhận</a></p>
				            <p><a href="/dieu-khoan-mua-ban-hang-hoa">Điều khoản Mua Bán Hàng Hoá</a></p>
				            <p><a href="/chinh-sach-doi-tra-san-pham" title="100 đêm nằm thử miễn phí tại Vua Nệm">Chính sách đổi trả sản phẩm</a></p>
				            <p><a href="/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></p>
				          </div>
				        </div>
				        <div class="benefit">
				          <div class="moit"><a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=46342" rel="noopener"><img src="/wp-content/uploads/footer/bocongthuong.svg" alt="bo-cong-thuong-logo"/></a></div>
				          <div class="copy-right">
				            <p>Mã số doanh nghiệp 0107968516 do Sở Kế hoạch Đầu tư Hà Nội cấp lần 1 ngày 18/8/2017</p>
				            <div class="payment-method">
				              <div><img src="/wp-content/uploads/footer/payment/icon-COD.svg" alt="COD-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/visa.svg" alt="visa-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/master-card.svg" alt="mastercard-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/america-express.svg" alt="america-express-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/JCB.svg" alt="JCB-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/onepay.svg" alt="onepay-logo"/></div>
				              <div><img src="/wp-content/uploads/footer/payment/VN-pay.svg" alt="VNPay-logo"/></div>
				            </div>
				          </div>
				        </div>
				      </div>
				    </div>
				  </div><!--  .section-inner -->

			</footer><!-- #site-footer -->
	</body>
</html>


<style>
.footer-nav-widgets-wrapper.header-footer-group{
	display: none !important;
}
.page-footer #bottomfooter .cdz-footer-bottom-payment a img {
  width: 92px;
}
@media (min-width: 768px) {
  .page-footer {
    display: none;
  }
}
#footer-vuanem {
  margin-top: 30px;
  background: #fff;
  padding: 25px 0 10px;
}
#footer-vuanem .container {
	padding: 0 5vw;
	margin: 0 auto;
}
@media (min-width: 1200px) {
  #footer-vuanem .container {
 	max-width: 1600px;
  }
}
#footer-vuanem .top-footer p {
  margin-top: 0.5em !important;
  margin-bottom: 1em !important;
}
#footer-vuanem .top-footer .title {
  height: 34px;
  color: #2d2e7f;
  font-size: 32px;
  font-weight: 600;
  text-align: center;
  width: 100%;
  margin-top: 15px;
}
#footer-vuanem .top-footer .benefit {
  width: 100%;
  display: flex;
}
#footer-vuanem .top-footer .benefit .benefit-item {
  width: 25%;
  margin: 30px 15px;
  text-align: center;
}
#footer-vuanem .top-footer .benefit .benefit-item .detail {
  color: #20315c;
  font-size: 14px;
}
#footer-vuanem .top-footer .benefit .benefit-item p {
  color: #20315c;
  font-size: 20px;
}
#footer-vuanem .top-footer .benefit .benefit-item p span {
  font-weight: 600;
}
#footer-vuanem .top-footer .benefit .benefit-item a {
  text-decoration: none !important;
  color: #2d2e7f !important;
}
#footer-vuanem .top-footer .benefit div:not(:nth-child(2)).free-trial-image {
  position: relative;
  top: -5px;
}
#footer-vuanem .bottom-footer {
  color: #606060;
  border-top: 1px solid #dadafa;
  padding: 40px 0 30px 0;
}
#footer-vuanem .bottom-footer .social {
  margin-top: 50px;
}
#footer-vuanem .bottom-footer .benefit {
  display: inline-block;
  width: 100%;
}
#footer-vuanem .bottom-footer .benefit .moit {
  width: 30%;
  display: inline-block;
}
#footer-vuanem .bottom-footer .benefit .moit img {
  width: 172px;
  height: 54px;
}
#footer-vuanem .bottom-footer .copy-right {
  display: inline-block;
}
#footer-vuanem .bottom-footer .copy-right .payment-method {
  display: flex;
  margin-top: 20px;
}
#footer-vuanem .bottom-footer .copy-right .payment-method div img {
  margin-right: 10px;
}
#footer-vuanem .bottom-footer .bottom-footer-container {
  width: 100%;
  padding-bottom: 35px;
  display: flex;
}
#footer-vuanem .bottom-footer .bottom-footer-container a {
  color: #606060;
  text-decoration: none;
}
#footer-vuanem .bottom-footer .bottom-footer-container a:hover {
  text-decoration: none;
  color: #2d2e7f !important;
}
#footer-vuanem .bottom-footer .bottom-footer-container span {
  color: #20315c;
}
#footer-vuanem .bottom-footer .bottom-footer-container span a {
  color: #20315c !important;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item {
  width: 23%;
  margin: 10px;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .title-bottom {
  margin-block-end: 1em;
  font-weight: 600;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .social-symbol {
  display: flex;
  width: 100%;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .social-symbol .fb-symbol i.fa {
  padding: 0.5em 0.8em;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .social-symbol i.fa {
  display: inline-block;
  border-radius: 60px;
  border: 1px solid #606060;
  padding: 0.5em 0.6em;
  font-size: 16px;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .social-symbol i.fa:hover {
  border: 1px solid #2d2e7f;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item .social-symbol .symbol {
  margin-left: 9px;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item:not(:first-child) {
  margin-top: 50px;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item.report {
  width: 30%;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item.report p {
  font-size: 14px;
  margin-block-start: 0.5em;
  margin-block-end: 0.5em;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item.report p span {
  font-weight: 600;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item.report img {
  margin-bottom: 45px;
}
#footer-vuanem .bottom-footer .bottom-footer-container .bottom-footer-item span {
  font-size: 18px;
  font-weight: 600;
}
@media (max-width: 992px) {
  #footer-vuanem .top-footer .benefit div:last-child .image {
    margin-bottom: 0;
  }
}
@media (max-width: 767px) {
  #back-top {
    -webkit-transition: all 0.4s;
    -moz-transition: all 0.4s;
    -ms-transition: all 0.4s;
    -o-transition: all 0.4s;
    transition: all 0.4s;
  }
  #back-top.show-bar {
    -webkit-transform: translateY(-65px);
    -moz-transform: translateY(-65px);
    -ms-transform: translateY(-65px);
    -o-transform: translateY(-65px);
    transform: translateY(-65px);
  }
  #footer-vuanem .container {
    display: none;
  }
  #footer-vuanem .top-footer .benefit div:last-child .image {
    margin-bottom: 0;
  }
  .page-footer .footer-custom-block {
    padding: 10px 0;
  }
  .page-footer #topfooter .footer-content-top {
    display: none;
  }
  .page-footer .footer-container-wrapper {
    background: #fff;
    padding-bottom: 1px;
  }
  .page-footer .footer-container-wrapper .block-static-block .row > div .footer-box .h5, .page-footer .footer-container-wrapper .block-static-block .row > div .footer-box .link {
    font-style: normal;
    color: #525252;
    border-bottom: 1px solid #eaeaea;
    padding-bottom: 14px;
    font-size: 16px;
    font-weight: 400;
    display: block;
  }
  .page-footer .footer-container-wrapper .block-static-block .row > div .footer-box .h5:after {
    color: #2d2e7f;
  }
  .page-footer .footer-container-wrapper .block-static-block .row > div:first-child {
    padding: 5px 0 10px;
  }
  .page-footer .footer-container-wrapper .block-static-block .row > div .footer-social {
    display: none;
  }
  .page-footer #bottomfooter {
    border: none;
    padding: 0;
  }
  .page-footer #bottomfooter .copyright {
    border-top: 1px solid #EAEAEA;
    padding: 15px 0;
    border-bottom: 1px solid #EAEAEA;
  }
  .page-footer #bottomfooter .copyright .footer-mobile-intro {
    color: #2D2E7F;
    display: block;
    font-size: 18px;
    padding-bottom: 15px;
    font-weight: 600;
  }
  .page-footer #bottomfooter .cdz-footer-bottom-payment > img {
    display: none;
  }
  .page-footer #bottomfooter .cdz-footer-bottom-payment a img {
    width: auto;
  }
  .page-footer #bottomfooter .paying-images {
    padding-bottom: 20px;
    display: block;
    margin: 0 auto;
  }
}
/* Icon Landing */
#vuanem-icon-landing > div {
  width: 90px;
  height: 90px;
  position: fixed;
  z-index: 999;
  bottom: 120px;
  left: 10%;
}
@media (max-width: 1680px) {
  #vuanem-icon-landing > div {
    left: 7%;
  }
}
@media (max-width: 1600px) {
  #vuanem-icon-landing > div {
    left: 5%;
  }
}
@media (max-width: 1440px) {
  #vuanem-icon-landing > div {
    left: 15px;
  }
}
#vuanem-icon-landing > div p {
  margin: 0;
}

</style>
