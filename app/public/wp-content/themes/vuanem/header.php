<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Vuanem
 * @since Vuanem 1.0
 */
?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<?php wp_head(); ?>
		<script id="gmt" src="https://www.googletagmanager.com/gtag/js?id=AW-720552318"></script><script async="" src="//www.gstatic.com/call-tracking/call-tracking_2.js" nonce="null"></script><script type="text/javascript" async="" src="https://www.gstatic.com/wcm/loader.js"></script><script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/gtm/js?id=GTM-5BRTPXC&amp;t=gtm2&amp;cid=1756413262.1605868567"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ec.js"></script><script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=AW-696364875&amp;l=dataLayer&amp;cx=c"></script><script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=AW-683976844&amp;l=dataLayer&amp;cx=c"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=G-V1LLJFBLQQ&amp;l=dataLayer&amp;cx=c"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-NBX6BWH"></script><script>
    var BASE_URL = 'https://vuanem.com/';
    var require = {
        "baseUrl": "https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN"
    };
</script>
<meta charset="utf-8">
<meta name="description" content="Vua Nệm - Hệ thống phân phối chăn ra gối nệm uy tín số 1 tại Việt nam. Truy cập ngay để nhận được dịch vụ tư vấn sản phẩm bởi các chuyên gia sức khỏe và giấc ngủ, từ đó lựa chọn được sản phẩm phù hợp nhất.">
<meta name="keywords" content="nệm giá rẻ, nệm nhập khẩu, nệm cao cấp, nem cao cap, nệm, ra gối, chăn ra gối nệm, chăn ga gối đệm">
<meta name="robots" content="NOINDEX, NOFOLLOW">
<meta name="viewport" content="width=device-width, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<title>Layout</title>
<link rel="stylesheet" type="text/css" media="all" href="https://vuanem.com/static/version1610533911/_cache/merged/399afdfae9b9b197d6de9240e9c37d3b.min.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/css/styles-l.min.css">
<link rel="stylesheet" type="text/css" media="all" href="https://vuanem.com/static/version1610533911/_cache/merged/6b1c14b780c7cee15185afbbbab48c52.min.css">
<link rel="stylesheet" type="text/css" href="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/js/dist/css/main.4bd18e37066e513ba1224b5052b4f16a.min.css">
<script type="text/javascript" src="https://vuanem.com/static/version1610533911/_cache/merged/458f811f31a49870978dcfda4107f096.js"></script>
<!--[if IE 11]>
<script  type="text/javascript"  src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/Smile_Map/js/polyfill/ie11/promise.min.js"></script>
<![endif]-->
<link rel="canonical" href="https://vuanem.com/layout/">
<link rel="icon" type="image/x-icon" href="https://vuanem.com/media/favicon/stores/1/favicon_1.ico">
<link rel="shortcut icon" type="image/x-icon" href="https://vuanem.com/media/favicon/stores/1/favicon_1.ico">
<meta name="cf-2fa-verify" content="8308ee092e12bbc">
<meta http-equiv="x-dns-prefetch-control" content="on">
<meta name="copyright" content="Công ty Cổ phần Vua Nệm">
<meta name="author" content="Công ty Cổ phần Vua Nệm">
<meta name="resource-type" content="Document">
<meta name="distribution" content="Global">
<meta name="revisit-after" content="1 days">
<link rel="dns-prefetch" href="https://media.vuanem.com/">
<meta property="og:locale" content="vi_VN">
<link rel="dns-prefetch" href="https://static.vuanem.com/">
<link rel="manifest" href="/manifest.json">
<style>
.cms-home #sidebarHotDeal, #location-festival {
display: none;
}
#vuanem-flash-sales .hot-deals {
    background: url('https://media.vuanem.com/wysiwyg/bg-flash-sale.jpg') !important;
}
.admin__data-grid-outer-wrap {
display: none;
}
.live-chat .items-action:hover .title-action {
    width: 160px;
}
.cdz-product-top img {opacity: 1;}
#product-detail-main-content .bottom-bar .items-block .button-quickcheckout {
margin: 0;
    border-color: #f50102;}
.payment-homecredit {
    display:none !important;
}

#mini-cart-block .minicart .mini-cart-items .price-container {
    background: #fff;
}
li.greet.welcome {
    display: none !important;
}

li.link.wishlist {
    display: none !important;
}
@media (min-width: 786px) {
#product-detail-main-content .product-detail-first-block .product-detail .product.attibute.overview .items-option:last-child {
padding-right: 30px;
}
}

#vuanem-icon-landing div{
    background-size: 20px;
    border-radius: 100%;
    -webkit-animation: phonering-alo-circle-img-anim 1.5s infinite ease-in-out;
    animation: phonering-alo-circle-img-anim 1.5s infinite ease-in-out;
    -webkit-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    align-items: center;
    justify-content: center;
}
@-webkit-keyframes phonering-alo-circle-anim {0% {-webkit-transform: rotate(0) scale(0.5) skew(1deg);-webkit-opacity: 0.1;}30% {-webkit-transform: rotate(0) scale(0.7) skew(1deg);-webkit-opacity: 0.5;}100% {-webkit-transform: rotate(0) scale(1) skew(1deg);-webkit-opacity: 0.1;}
}
@-webkit-keyframes phonering-alo-circle-fill-anim {0% {-webkit-transform: rotate(0) scale(0.7) skew(1deg);opacity: 0.6;}50% {-webkit-transform: rotate(0) scale(1) skew(1deg);opacity: 0.6;}100% {-webkit-transform: rotate(0) scale(0.7) skew(1deg);opacity: 0.6;}
}
@-webkit-keyframes phonering-alo-circle-img-anim {0% {-webkit-transform: rotate(0) scale(1) skew(1deg);}10% {-webkit-transform: rotate(-25deg) scale(1) skew(1deg);}20% {-webkit-transform: rotate(25deg) scale(1) skew(1deg);}30% {-webkit-transform: rotate(-25deg) scale(1) skew(1deg);}40% {-webkit-transform: rotate(25deg) scale(1) skew(1deg);}50% {-webkit-transform: rotate(0) scale(1) skew(1deg);}100% {-webkit-transform: rotate(0) scale(1) skew(1deg);}
}
.customweb-sagepaycw-payment-form {display: none;}
</style>
<meta name="google-site-verification" content="BverUIezZQ0MFs7T9Xo6dw0df_VB8-0OhVc1hRbd9GQ">
<meta name="msvalidate.01" content="652F55E5C0DCB2723C56C8B1C65B68D3"> <script>
        window.getWpCookie = function(name) {
            match = document.cookie.match(new RegExp(name + '=([^;]+)'));
            if (match) return decodeURIComponent(match[1].replace(/\+/g, ' ')) ;
        };

        window.dataLayer = window.dataLayer || [];
            </script>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NBX6BWH');</script>
<script>var site_url = BASE_URL;</script><script type="text/javascript" src="https://chimpstatic.com/mcjs-connected/js/users/8a08b45093087cf4980625567/e7e338a53630949d39581ebc7.js" async=""></script>
<style>
    .block.social-login-authentication-channel.account-social-login .block-content { text-align: center; }
	</style>
<style>
	.magezon-builder .mgz-container {width: 1260px;}
	</style>
<script type="text/javascript">
	var ThemeOptions = {
		box_wide: 0,
		rtl_layout: 0,
		sticky_header: 1		
	}; 
	if(navigator.userAgent.match(/iPhone|iPod|iPhone Simulator|iPod Simulator/i) !== null){
		document.addEventListener("DOMContentLoaded", function(event) { 
			document.body.classList.add('iMenu');
		});
	}
</script>
<link rel="preload" href="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/fonts/Luma-Icons.woff2"><script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","url":"https:\/\/vuanem.com\/","name":"Vua N\u1ec7m"}</script><script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"WebPage","speakable":{"@type":"SpeakableSpecification","cssSelector":[".cms-content"],"xpath":["\/html\/head\/title"]}}</script> <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="mage/requirejs/text" src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/mage/requirejs/text.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="js/jquery-migrate" src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/js/jquery-migrate.js"></script><script src="https://cdn.onesignal.com/sdks/OneSignalPageSDKES6.js?v=150705" async=""></script><script type="text/javascript" charset="utf-8" async="" defer="" src="https://front.optimonk.com/public/118686/js/preload.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="moment" src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/moment.js"></script><style type="text/css">
.VueCarousel-navigation-button[data-v-453ad8cd] {
  position: absolute;
  top: 50%;
  box-sizing: border-box;
  color: #000;
  text-decoration: none;
  appearance: none;
  border: none;
  background-color: transparent;
  padding: 0;
  cursor: pointer;
  outline: none;
}
.VueCarousel-navigation-button[data-v-453ad8cd]:focus {
  outline: 1px solid lightblue;
}
.VueCarousel-navigation-next[data-v-453ad8cd] {
  right: 0;
  transform: translateY(-50%) translateX(100%);
  font-family: "system";
}
.VueCarousel-navigation-prev[data-v-453ad8cd] {
  left: 0;
  transform: translateY(-50%) translateX(-100%);
  font-family: "system";
}
.VueCarousel-navigation--disabled[data-v-453ad8cd] {
  opacity: 0.5;
  cursor: default;
}

/* Define the "system" font family */
@font-face {
  font-family: system;
  font-style: normal;
  font-weight: 300;
  src: local(".SFNSText-Light"), local(".HelveticaNeueDeskInterface-Light"),
    local(".LucidaGrandeUI"), local("Ubuntu Light"), local("Segoe UI Symbol"),
    local("Roboto-Light"), local("DroidSans"), local("Tahoma");
}
</style><style type="text/css">
.VueCarousel-pagination[data-v-438fd353] {
  width: 100%;
  text-align: center;
}
.VueCarousel-pagination--top-overlay[data-v-438fd353] {
  position: absolute;
  top: 0;
}
.VueCarousel-pagination--bottom-overlay[data-v-438fd353] {
  position: absolute;
  bottom: 0;
}
.VueCarousel-dot-container[data-v-438fd353] {
  display: inline-block;
  margin: 0 auto;
  padding: 0;
}
.VueCarousel-dot[data-v-438fd353] {
  display: inline-block;
  cursor: pointer;
  appearance: none;
  border: none;
  background-clip: content-box;
  box-sizing: content-box;
  padding: 0;
  border-radius: 100%;
  outline: none;
}
.VueCarousel-dot[data-v-438fd353]:focus {
  outline: 1px solid lightblue;
}
</style><style type="text/css">
.VueCarousel-slide {
  flex-basis: inherit;
  flex-grow: 0;
  flex-shrink: 0;
  user-select: none;
  backface-visibility: hidden;
  -webkit-touch-callout: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: none;
}
.VueCarousel-slide-adjustableHeight {
  display: table;
  flex-basis: auto;
  width: 100%;
}
</style><style type="text/css">
.VueCarousel {
  display: flex;
  flex-direction: column;
  position: relative;
}
.VueCarousel--reverse {
  flex-direction: column-reverse;
}
.VueCarousel-wrapper {
  width: 100%;
  position: relative;
  overflow: hidden;
}
.VueCarousel-inner {
  display: flex;
  flex-direction: row;
  backface-visibility: hidden;
}
.VueCarousel-inner--center {
  justify-content: center;
}
</style>
<script type="text/javascript"></script><meta http-equiv="origin-trial" content="AxL6K+7uLMunmYb1wpp/pnDPBsoQgMGPaTDkE6HIAKvHNoXilVyfzrIZM9q7OUqMUDJCYgIlFmJcvooS3kGjowMAAACOeyJvcmlnaW4iOiJodHRwczovL3d3dy5nb29nbGVhZHNlcnZpY2VzLmNvbTo0NDMi
LCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjE0MTI0Nzk5LCJpc1N1YmRvbWFpbiI6dHJ1ZSwiaXNUaGlyZFBhcnR5Ijp0cnVlfQ==">
<script src="https://www.googleadservices.com/pagead/conversion/696364875/?random=1611806085850&amp;cv=9&amp;fst=1611806085850&amp;num=1&amp;value=0&amp;label
=9_eyCMCtrrUBEMvehswC2&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=1050&amp;u_w=1680&amp;u_ah=977&amp;u_aw=1680&amp;u_cd=24&amp;u_his=6&amp;u_tz=420&amp;u
_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2wg1k0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Fvuanem.com%2Flayout&amp;tiba=Layout&amp;hn=www
.googleadservices.com&amp;bttype=purchase&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>
<script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/683976844/?random=1611806085883&amp;
cv=9&amp;fst=1611806085883&amp;num=1&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=1050&amp;u_w=1680&amp;u_ah=977&amp;u_aw=1680&amp;u_cd=24
&amp;u_his=6&amp;u_tz=420&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2wg1k0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Fvuane
m.com%2Flayout&amp;tiba=Layout&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><script type="text/javascript" charset=
"utf-8" async="" data-requirecontext="_" data-requiremodule="mage/requirejs/resolver" src="https://vuanem.com/static/version1610533911/frontend/Co
dazon/fastest_furniture/vi_VN/mage/requirejs/resolver.js"></script>
<script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/683976844/?random=1611806087615&amp;cv=9&amp;fst=1611806087615&
amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=1050&amp;u_w=1680&amp;u_ah=977&amp;u_aw=1680&amp;u_cd=24&amp;u_his=6&amp;u_tz=420&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2oa1k0&amp;sendb=1&amp;ig=1&amp;data=event%3Dgtag.config&amp;frm=0&amp;url=https%3A%2F%2Fvuanem.com%2Flayout&amp;tiba=Layout&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/696364875/?random=1611806087627&amp;cv=9&amp;fst=1611806087627&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=1050&amp;u_w=1680&amp;u_ah=977&amp;u_aw=1680&amp;u_cd=24&amp;u_his=6&amp;u_tz=420&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2oa1k0&amp;sendb=1&amp;ig=1&amp;data=event%3Dgtag.config&amp;frm=0&amp;url=https%3A%2F%2Fvuanem.com%2Flayout&amp;tiba=Layout&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><script async="" src="https://static.hotjar.com/c/hotjar-1452858.js?sv=6"></script><script async="" src="https://a.omappapi.com/app/js/api.min.js?ver=6" id="popup-remote" data-account="91740" data-user="81554"></script><script async="" src="https://script.hotjar.com/modules.59fae23e8e8310b9fca6.js" charset="utf-8"></script><style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style><style type="text/css">@-webkit-keyframes omBounce{0%,20%,50%,80%,to{-webkit-transform:translateY(0);transform:translateY(0)}40%{-webkit-transform:translateY(-30px);transform:translateY(-30px)}60%{-webkit-transform:translateY(-15px);transform:translateY(-15px)}}@keyframes omBounce{0%,20%,50%,80%,to{-webkit-transform:translateY(0);transform:translateY(0)}40%{-webkit-transform:translateY(-30px);transform:translateY(-30px)}60%{-webkit-transform:translateY(-15px);transform:translateY(-15px)}}.om-animation-bounce{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:omBounce;animation-name:omBounce}@-webkit-keyframes omBounceIn{0%{opacity:0;-webkit-transform:scale(.3);transform:scale(.3)}50%{opacity:1;-webkit-transform:scale(1.05);transform:scale(1.05)}70%{-webkit-transform:scale(.9);transform:scale(.9)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes omBounceIn{0%{opacity:0;-webkit-transform:scale(.3);transform:scale(.3)}50%{opacity:1;-webkit-transform:scale(1.05);transform:scale(1.05)}70%{-webkit-transform:scale(.9);transform:scale(.9)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}.om-animation-bounce-in{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omBounceIn;animation-name:omBounceIn}@-webkit-keyframes omBounceInDown{0%{opacity:0;-webkit-transform:translateY(-2000px);transform:translateY(-2000px)}60%{opacity:1;-webkit-transform:translateY(30px);transform:translateY(30px)}80%{-webkit-transform:translateY(-10px);transform:translateY(-10px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes omBounceInDown{0%{opacity:0;-webkit-transform:translateY(-2000px);transform:translateY(-2000px)}60%{opacity:1;-webkit-transform:translateY(30px);transform:translateY(30px)}80%{-webkit-transform:translateY(-10px);transform:translateY(-10px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}.om-animation-bounce-in-down{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omBounceInDown;animation-name:omBounceInDown}@-webkit-keyframes omBounceInLeft{0%{opacity:0;-webkit-transform:translateX(-2000px);transform:translateX(-2000px)}60%{opacity:1;-webkit-transform:translateX(30px);transform:translateX(30px)}80%{-webkit-transform:translateX(-10px);transform:translateX(-10px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}@keyframes omBounceInLeft{0%{opacity:0;-webkit-transform:translateX(-2000px);transform:translateX(-2000px)}60%{opacity:1;-webkit-transform:translateX(30px);transform:translateX(30px)}80%{-webkit-transform:translateX(-10px);transform:translateX(-10px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}.om-animation-bounce-in-left{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omBounceInLeft;animation-name:omBounceInLeft}@-webkit-keyframes omBounceInRight{0%{opacity:0;-webkit-transform:translateX(2000px);transform:translateX(2000px)}60%{opacity:1;-webkit-transform:translateX(-30px);transform:translateX(-30px)}80%{-webkit-transform:translateX(10px);transform:translateX(10px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}@keyframes omBounceInRight{0%{opacity:0;-webkit-transform:translateX(2000px);transform:translateX(2000px)}60%{opacity:1;-webkit-transform:translateX(-30px);transform:translateX(-30px)}80%{-webkit-transform:translateX(10px);transform:translateX(10px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}.om-animation-bounce-in-right{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omBounceInRight;animation-name:omBounceInRight}@-webkit-keyframes omBounceInUp{0%{opacity:0;-webkit-transform:translateY(2000px);transform:translateY(2000px)}60%{opacity:1;-webkit-transform:translateY(-30px);transform:translateY(-30px)}80%{-webkit-transform:translateY(10px);transform:translateY(10px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes omBounceInUp{0%{opacity:0;-webkit-transform:translateY(2000px);transform:translateY(2000px)}60%{opacity:1;-webkit-transform:translateY(-30px);transform:translateY(-30px)}80%{-webkit-transform:translateY(10px);transform:translateY(10px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}.om-animation-bounce-in-up{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omBounceInUp;animation-name:omBounceInUp}@-webkit-keyframes omFlash{0%,50%,to{opacity:1}25%,75%{opacity:0}}@keyframes omFlash{0%,50%,to{opacity:1}25%,75%{opacity:0}}.om-animation-flash{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:omFlash;animation-name:omFlash}@-webkit-keyframes omFlip{0%{-webkit-transform:perspective(800px) translateZ(0) rotateY(0) scale(1);transform:perspective(800px) translateZ(0) rotateY(0) scale(1);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}40%{-webkit-transform:perspective(800px) translateZ(150px) rotateY(170deg) scale(1);transform:perspective(800px) translateZ(150px) rotateY(170deg) scale(1);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}50%{-webkit-transform:perspective(800px) translateZ(150px) rotateY(190deg) scale(1);transform:perspective(800px) translateZ(150px) rotateY(190deg) scale(1);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}80%{-webkit-transform:perspective(800px) translateZ(0) rotateY(1turn) scale(.95);transform:perspective(800px) translateZ(0) rotateY(1turn) scale(.95);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}to{-webkit-transform:perspective(800px) translateZ(0) rotateY(1turn) scale(1);transform:perspective(800px) translateZ(0) rotateY(1turn) scale(1);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}}@keyframes omFlip{0%{-webkit-transform:perspective(800px) translateZ(0) rotateY(0) scale(1);transform:perspective(800px) translateZ(0) rotateY(0) scale(1);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}40%{-webkit-transform:perspective(800px) translateZ(150px) rotateY(170deg) scale(1);transform:perspective(800px) translateZ(150px) rotateY(170deg) scale(1);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}50%{-webkit-transform:perspective(800px) translateZ(150px) rotateY(190deg) scale(1);transform:perspective(800px) translateZ(150px) rotateY(190deg) scale(1);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}80%{-webkit-transform:perspective(800px) translateZ(0) rotateY(1turn) scale(.95);transform:perspective(800px) translateZ(0) rotateY(1turn) scale(.95);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}to{-webkit-transform:perspective(800px) translateZ(0) rotateY(1turn) scale(1);transform:perspective(800px) translateZ(0) rotateY(1turn) scale(1);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}}.om-animation-flip{-webkit-animation-duration:1s;animation-duration:1s;-webkit-backface-visibility:visible;backface-visibility:visible;-webkit-animation-name:omFlip;animation-name:omFlip}@-webkit-keyframes omFlipInX{0%{-webkit-transform:perspective(800px) rotateX(90deg);transform:perspective(800px) rotateX(90deg);opacity:0}40%{-webkit-transform:perspective(800px) rotateX(-10deg);transform:perspective(800px) rotateX(-10deg)}70%{-webkit-transform:perspective(800px) rotateX(10deg);transform:perspective(800px) rotateX(10deg)}to{-webkit-transform:perspective(800px) rotateX(0deg);transform:perspective(800px) rotateX(0deg);opacity:1}}@keyframes omFlipInX{0%{-webkit-transform:perspective(800px) rotateX(90deg);transform:perspective(800px) rotateX(90deg);opacity:0}40%{-webkit-transform:perspective(800px) rotateX(-10deg);transform:perspective(800px) rotateX(-10deg)}70%{-webkit-transform:perspective(800px) rotateX(10deg);transform:perspective(800px) rotateX(10deg)}to{-webkit-transform:perspective(800px) rotateX(0deg);transform:perspective(800px) rotateX(0deg);opacity:1}}.om-animation-flip-down{-webkit-animation-duration:1s;animation-duration:1s;-webkit-backface-visibility:visible;backface-visibility:visible;-webkit-animation-name:omFlipInX;animation-name:omFlipInX}@-webkit-keyframes omFlipInY{0%{-webkit-transform:perspective(800px) rotateY(90deg);transform:perspective(800px) rotateY(90deg);opacity:0}40%{-webkit-transform:perspective(800px) rotateY(-
10deg);transform:perspective(800px) rotateY(-10deg)}70%{-webkit-transform:perspective(800px) rotateY(10deg);transform:perspective(800px) rotateY(10deg)}to{-webkit-transform:perspective(800px) rotateY(0deg);transform:perspective(800px) rotateY(0deg);opacity:1}}@keyframes omFlipInY{0%{-webkit-transform:perspective(800px) rotateY(90deg);transform:perspective(800px) rotateY(90deg);opacity:0}40%{-webkit-transform:perspective(800px) rotateY(-10deg);transform:perspective(800px) rotateY(-10deg)}70%{-webkit-transform:perspective(800px) rotateY(10deg);transform:perspective(800px) rotateY(10deg)}to{-webkit-transform:perspective(800px) rotateY(0deg);transform:perspective(800px) rotateY(0deg);opacity:1}}.om-animation-flip-side{-webkit-animation-duration:1s;animation-duration:1s;-webkit-backface-visibility:visible;backface-visibility:visible;-webkit-animation-name:omFlipInY;animation-name:omFlipInY}@-webkit-keyframes omLightSpeedIn{0%{-webkit-transform:translateX(100%) skewX(-30deg);transform:translateX(100%) skewX(-30deg);opacity:0}60%{-webkit-transform:translateX(-20%) skewX(30deg);transform:translateX(-20%) skewX(30deg);opacity:1}80%{-webkit-transform:translateX(0) skewX(-15deg);transform:translateX(0) skewX(-15deg);opacity:1}to{-webkit-transform:translateX(0) skewX(0deg);transform:translateX(0) skewX(0deg);opacity:1}}@keyframes omLightSpeedIn{0%{-webkit-transform:translateX(100%) skewX(-30deg);transform:translateX(100%) skewX(-30deg);opacity:0}60%{-webkit-transform:translateX(-20%) skewX(30deg);transform:translateX(-20%) skewX(30deg);opacity:1}80%{-webkit-transform:translateX(0) skewX(-15deg);transform:translateX(0) skewX(-15deg);opacity:1}to{-webkit-transform:translateX(0) skewX(0deg);transform:translateX(0) skewX(0deg);opacity:1}}.om-animation-light-speed{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omLightSpeedIn;animation-name:omLightSpeedIn;-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}@-webkit-keyframes omPulse{0%{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(1.1);transform:scale(1.1)}to{-webkit-transform:scale(1);transform:scale(1)}}@keyframes omPulse{0%{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(1.1);transform:scale(1.1)}to{-webkit-transform:scale(1);transform:scale(1)}}.om-animation-pulse{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:omPulse;animation-name:omPulse}@-webkit-keyframes omRollIn{0%{opacity:0;-webkit-transform:translateX(-100%) rotate(-120deg);transform:translateX(-100%) rotate(-120deg)}to{opacity:1;-webkit-transform:translateX(0) rotate(0deg);transform:translateX(0) rotate(0deg)}}@keyframes omRollIn{0%{opacity:0;-webkit-transform:translateX(-100%) rotate(-120deg);transform:translateX(-100%) rotate(-120deg)}to{opacity:1;-webkit-transform:translateX(0) rotate(0deg);transform:translateX(0) rotate(0deg)}}.om-animation-roll-in{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRollIn;animation-name:omRollIn}@-webkit-keyframes omRotateIn{0%{-webkit-transform-origin:center center;transform-origin:center center;-webkit-transform:rotate(-200deg);transform:rotate(-200deg);opacity:0}to{-webkit-transform-origin:center center;transform-origin:center center;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes omRotateIn{0%{-webkit-transform-origin:center center;transform-origin:center center;-webkit-transform:rotate(-200deg);transform:rotate(-200deg);opacity:0}to{-webkit-transform-origin:center center;transform-origin:center center;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}.om-animation-rotate{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRotateIn;animation-name:omRotateIn}@-webkit-keyframes omRotateInDownLeft{0%{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(-90deg);transform:rotate(-90deg);opacity:0}to{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes omRotateInDownLeft{0%{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(-90deg);transform:rotate(-90deg);opacity:0}to{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}.om-animation-rotate-down-left{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRotateInDownLeft;animation-name:omRotateInDownLeft}@-webkit-keyframes omRotateInDownRight{0%{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(90deg);transform:rotate(90deg);opacity:0}to{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes omRotateInDownRight{0%{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(90deg);transform:rotate(90deg);opacity:0}to{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}.om-animation-rotate-down-right{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRotateInDownRight;animation-name:omRotateInDownRight}@-webkit-keyframes omRotateInUpLeft{0%{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(90deg);transform:rotate(90deg);opacity:0}to{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes omRotateInUpLeft{0%{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(90deg);transform:rotate(90deg);opacity:0}to{-webkit-transform-origin:left bottom;transform-origin:left bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}.om-animation-rotate-up-left{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRotateInUpLeft;animation-name:omRotateInUpLeft}@-webkit-keyframes omRotateInUpRight{0%{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(-90deg);transform:rotate(-90deg);opacity:0}to{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes omRotateInUpRight{0%{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(-90deg);transform:rotate(-90deg);opacity:0}to{-webkit-transform-origin:right bottom;transform-origin:right bottom;-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}.om-animation-rotate-up-right{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omRotateInUpRight;animation-name:omRotateInUpRight}@-webkit-keyframes omRubberBand{0%{-webkit-transform:scale(1);transform:scale(1)}30%{-webkit-transform:scaleX(1.25) scaleY(.75);transform:scaleX(1.25) scaleY(.75)}40%{-webkit-transform:scaleX(.75) scaleY(1.25);transform:scaleX(.75) scaleY(1.25)}60%{-webkit-transform:scaleX(1.15) scaleY(.85);transform:scaleX(1.15) scaleY(.85)}to{-webkit-transform:scale(1);transform:scale(1)}}@keyframes omRubberBand{0%{-webkit-transform:scale(1);transform:scale(1)}30%{-webkit-transform:scaleX(1.25) scaleY(.75);transform:scaleX(1.25) scaleY(.75)}40%{-webkit-transform:scaleX(.75) scaleY(1.25);transform:scaleX(.75) scaleY(1.25)}60%{-webkit-transform:scaleX(1.15) scaleY(.85);transform:scaleX(1.15) scaleY(.85)}to{-webkit-transform:scale(1);transform:scale(1)}}.om-animation-rubber-band{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:omRubberBand;animation-name:omRubberBand}@-webkit-keyframes omShake{0%,to{-webkit-transform:translateX(0);transform:translateX(0)}10%,30%,50%,70%,90%{-webkit-transform:translateX(-10px);transform:translateX(-10px)}20%,40%,60%,80%{-webkit-transform:translateX(10px);transform:translateX(10px)}}@keyframes omShake{0%,to{-webkit-transform:translateX(0);transform:translateX(0)}10%,30%,50%,70%,90%{-webkit-transform:translateX(-10px);transform:translateX(-10px)}20%,40%,60%,80%{-webkit-transform:translateX(10px);transform:translateX(10px)}}.om-animation-shake{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:omShake;animation-name:omShake}@-webkit-keyframes omSlideInDown{0%{opacity:0;-webkit-transform:translateY(-2000px);transform:translateY(-2000px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes omSlideInDown{0%{opacity:0;-webkit-transform:translateY(-2000px);transform:translateY(-2000px)}to{-webkit-transform:translateY(0);transform:translateY(0)}}.om-animation-slide-in-down{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omSlideInDown;animation-name:omSlideInDown}@-webkit-keyframes omSlideInLeft{0%{opacity:0;-webkit-transform:translateX(-2000px);transform:translateX(-2000px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}
@keyframes omSlideInLeft{0%{opacity:0;-webkit-transform:translateX(-2000px);transform:translateX(-2000px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}.om-animation-slide-in-left{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omSlideInLeft;animation-name:omSlideInLeft}@-webkit-keyframes omSlideInRight{0%{opacity:0;-webkit-transform:translateX(2000px);transform:translateX(2000px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}@keyframes omSlideInRight{0%{opacity:0;-webkit-transform:translateX(2000px);transform:translateX(2000px)}to{-webkit-transform:translateX(0);transform:translateX(0)}}.om-animation-slide-in-right{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omSlideInRight;animation-name:omSlideInRight}@-webkit-keyframes omSwing{20%{-webkit-transform:rotate(15deg);transform:rotate(15deg)}40%{-webkit-transform:rotate(-10deg);transform:rotate(-10deg)}60%{-webkit-transform:rotate(5deg);transform:rotate(5deg)}80%{-webkit-transform:rotate(-5deg);transform:rotate(-5deg)}to{-webkit-transform:rotate(0deg);transform:rotate(0deg)}}@keyframes omSwing{20%{-webkit-transform:rotate(15deg);transform:rotate(15deg)}40%{-webkit-transform:rotate(-10deg);transform:rotate(-10deg)}60%{-webkit-transform:rotate(5deg);transform:rotate(5deg)}80%{-webkit-transform:rotate(-5deg);transform:rotate(-5deg)}to{-webkit-transform:rotate(0deg);transform:rotate(0deg)}}.om-animation-swing{-webkit-animation-duration:1s;animation-duration:1s;-webkit-transform-origin:top center;transform-origin:top center;-webkit-animation-name:omSwing;animation-name:omSwing}@-webkit-keyframes omTada{0%{-webkit-transform:scale(1);transform:scale(1)}10%,20%{-webkit-transform:scale(.9) rotate(-3deg);transform:scale(.9) rotate(-3deg)}30%,50%,70%,90%{-webkit-transform:scale(1.1) rotate(3deg);transform:scale(1.1) rotate(3deg)}40%,60%,80%{-webkit-transform:scale(1.1) rotate(-3deg);transform:scale(1.1) rotate(-3deg)}to{-webkit-transform:scale(1) rotate(0);transform:scale(1) rotate(0)}}@keyframes omTada{0%{-webkit-transform:scale(1);transform:scale(1)}10%,20%{-webkit-transform:scale(.9) rotate(-3deg);transform:scale(.9) rotate(-3deg)}30%,50%,70%,90%{-webkit-transform:scale(1.1) rotate(3deg);transform:scale(1.1) rotate(3deg)}40%,60%,80%{-webkit-transform:scale(1.1) rotate(-3deg);transform:scale(1.1) rotate(-3deg)}to{-webkit-transform:scale(1) rotate(0);transform:scale(1) rotate(0)}}.om-animation-tada{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omTada;animation-name:omTada}@-webkit-keyframes omWobble{0%{-webkit-transform:translateX(0);transform:translateX(0)}15%{-webkit-transform:translateX(-25%) rotate(-5deg);transform:translateX(-25%) rotate(-5deg)}30%{-webkit-transform:translateX(20%) rotate(3deg);transform:translateX(20%) rotate(3deg)}45%{-webkit-transform:translateX(-15%) rotate(-3deg);transform:translateX(-15%) rotate(-3deg)}60%{-webkit-transform:translateX(10%) rotate(2deg);transform:translateX(10%) rotate(2deg)}75%{-webkit-transform:translateX(-5%) rotate(-1deg);transform:translateX(-5%) rotate(-1deg)}to{-webkit-transform:translateX(0);transform:translateX(0)}}@keyframes omWobble{0%{-webkit-transform:translateX(0);transform:translateX(0)}15%{-webkit-transform:translateX(-25%) rotate(-5deg);transform:translateX(-25%) rotate(-5deg)}30%{-webkit-transform:translateX(20%) rotate(3deg);transform:translateX(20%) rotate(3deg)}45%{-webkit-transform:translateX(-15%) rotate(-3deg);transform:translateX(-15%) rotate(-3deg)}60%{-webkit-transform:translateX(10%) rotate(2deg);transform:translateX(10%) rotate(2deg)}75%{-webkit-transform:translateX(-5%) rotate(-1deg);transform:translateX(-5%) rotate(-1deg)}to{-webkit-transform:translateX(0);transform:translateX(0)}}.om-animation-wobble{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-name:omWobble;animation-name:omWobble}.om-content-lock{color:transparent!important;text-shadow:rgba(0,0,0,.5) 0 0 10px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;pointer-events:none;filter:url("data:image/svg+xml;utf9,<svg%20version='1.1'%20xmlns='http://www.w3.org/2000/svg'><filter%20id='blur'><feGaussianBlur%20stdDeviation='10'%20/></filter></svg>#blur");-webkit-filter:blur(10px);-ms-filter:blur(10px);-o-filter:blur(10px);filter:blur(10px)}html.om-mobile-position,html.om-mobile-position body{position:fixed!important}html.om-ios-form,html.om-ios-form body{-webkit-transform:translateZ(0)!important;transform:translateZ(0)!important;-webkit-overflow-scrolling:touch!important;height:100%!important;overflow:auto!important}html.om-position-popup body{overflow:hidden!important}html.om-position-floating-top{transition:padding-top .5s ease!important}html.om-position-floating-bottom{transition:padding-bottom .5s ease!important}html.om-reset-dimensions{height:100%!important;width:100%!important}.om-verification-confirmation{font-family:Lato,Arial,Helvetica,sans-serif;position:fixed;border-radius:10px;bottom:20px;left:20px;padding:10px 20px;opacity:0;transition:opacity .3s ease-in;background:#85bf31;color:#fff;font-size:18px;font-weight:700;z-index:9999}</style><script src="https://a.omappapi.com/app/js/webfont/1.5.18/webfont.js" async=""></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="momentTz" src="https://a.omappapi.com/app/js/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script><link rel="stylesheet" href="https://onesignal.com/sdks/OneSignalSDKStyles.css?v=2">
<div id="header-general"><div id="vuanem-header"><!----> <div class="header-default show-header"><div class="header-top"><div class="container"><div id="header-top-left " class="header-top-left desktop"><p><a href="https://vuanem.com/tra-gop-online"><img class="nomal-image" src="https://media.vuanem.com/wysiwyg/blog/Icon/khuyen-mai.png" alt=""><img class="hover-image" src="https://media.vuanem.com/wysiwyg/blog/Icon/ic-promotion-topbar-hover.png" alt=""> Trả góp Online đơn giản nhanh gọn </a></p>
<style xml="space"><!--
#om-igbvkzexpsm5mvuxwcuc{ right: 60px!important;}
.catalog-product-view .product-detail-product-info-detail .product-info-detailed .main-column .content-block{padding-bottom:15px;}
@media (min-width: 1200px){
.cms-home .product-listing.grid .item .product .left-content-info span.outstock {
    float: none;
    margin-left: 0;
    height: 52px;
    display: block;
    clear: both;
    width: 100%;
}
.product-listing .item .product .price-container .tra-gop-pr{    padding-left: 10px;
    margin-left: 10px;}
}
.banner-4image{display:none!Important}
.section-product .add-background-white .col-md-6 p{margin-bottom:0;}
.vuanem-blog-list {
    background-color: #fff;
    padding: 40px 0 30px!important;
}
#vuanem-slide-image,
#testimonial{display:none!important}

.cms-tra-gop-online .page-wrapper .page-main .widget.block.block-static-block .products-grid .product-items:not(.owl-carousel)>.product-item{width:25%;}
#vuanem-blog-list, #vuanem-flash-sales-new .loading-spin.container, .cdz-product-top, .item-caption p, .listing-block .image, .section-product .add-background-white, .section-tabs-brand .banner-image, .section-tabs-brand .section-brand, .slidemain-image .blog-wapper, .slider-image{background:#fff!important;}
#mattress-tab1 .item:after, #vuanem-blog-list:after, .cdz-product-top:after, .item-caption p:after, .listing-block .image:after, .loading-spin.container:after, .section-product .add-background-white:after, .section-tabs-brand .banner-image:after, .section-tabs-brand .section-brand:after, .slidemain-image .blog-wapper:after, .slider-image:after{opacity:0!important}
.loading-active-12 .slider-image{height:auto;}
body #vuanem-flash-sales-new{margin:0!important;}
#vuanem-flash-sales-new{}
#footer-vuanem .container{background:none}
.product-listing.grid .item .product .product-link .product-cover {
    padding-bottom: 110px;
}
.catalog-product-view .product-detail-product-info-detail .block-products-list .product-item {
    width: 100%!important;
}
body.open-menu::before {z-index:6}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top>.menu-link{min-width:52px;}
.cms-home .column.main{min-height:0}
#vuanem-header .hide-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(5)>.menu-link .menu-icon{display:none;}
#vuanem-flash-sales-new .container.loading-spin{min-height: inherit;    min-height: unset;}
#vuanem-flash-sales-new{min-height: inherit;    min-height: unset;}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(1)>.menu-link:before{content:"\E902"}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(2)>.menu-link:before{content:"\E900"}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(3)>.menu-link:before{content:"\E903"}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(4)>.menu-link:before{content:"\E901"}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(5)>.menu-link .menu-icon{display:block}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(5)>.menu-link:before{display:none!important}

#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top>.menu-link:before{height:29px;line-height:normal}

body #product-detail-main-content .product-detail-first-block .product-detail .product.attibute.overview .size-thick {
    width: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: reverse;
    -ms-flex-direction: row-reverse;
    flex-direction: row-reverse;
    border-bottom: 1px solid #ececec;
}
body #product-detail-main-content .product-detail-first-block .product-detail .product.attibute.overview .items-option:first-child{padding-right:0}
.icon-images-pr .freeship-product img{    min-width: 40px;}
.header-top-left a .nomal-image{z-index:1!Important}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .product-type-key-attributes .attribute-wapprer, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .product-type-key-attributes .attribute-wapprer{padding-left:20px!Important;padding-right:20!important}
 [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .product-item-info{width:100%}
#vuanem-product-filter .block.product-list .arrange-fill .view-detail{display:block!important}
#store-detail ul{overflow-y: scroll;}
.custom-sale-online{display:none!important}
#store-detail ul{height:160px;max-height:160px;}
.block-detail-top > div:last-child{
width:100%;
}
.product-item .freeship-product{display:none!important}
body #product-detail-main-content .product-detail-first-block .col-block-right{background: #f0f2f4;padding-top:0;    padding-bottom: 0;}
#product-detail-main-content .product-detail-first-block .col-block-right>div.detail-block-right-top{padding-top:15px;}
#product-detail-main-content .product-detail-first-block .col-block-right>div{background:#fff}
.custom-sale-online svg{width:80%}
.icon-images-pr{z-index:5}
.tooltip-free{display:none;}
.freeship-product:hover .tooltip-free{display:block;}
.catalog-product-view #product-detail-product-info-detail .product-review-bottom .load-more{height:30px;}
.catalog-product-view #product-detail-product-info-detail .product-review-bottom .load-more span{position:absolute;z-index:11;}
#product-detail-product-info-detail .product-item .active-freeship .freeship-product{display:none}
#product-detail-main-content .price-status.active-freeship .freeship-product{  color: #239B28; display:none;   font-size: 18px;font-weight: 600;}
.product-detail .hotline{display:none;}

#product-detail-product-info-detail .product-item-details .price-box .normal-price .price{     text-decoration: line-through;    font-size: 14px;color: #9a9a9a;}
.header-top .container:before, .header-top .container:after{display: none;}
@media (min-width: 768px){
#vuanem-header {
    height: 128px;
}
}
body .toggleOwl.mattress-tab-section {
    padding-top: 15px;
}
body.cms-home.cms-index-index .page-wrapper .page-main .widget.block.block-static-block .cdz-block-title {
    padding: 15px 20px 0;
}
@media (min-width: 768px) and (max-width: 1200px) { 
    body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content{width: 60%!important;}
    body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info, 
    body[class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info{border-right: none;}
}
.banner2 > .row{padding-bottom: 30px;
    border-bottom: 1px solid #eaeaea;}
body .information-product .section-title{z-index:1}
#vuanem-product-filter .block.product-list .filter-list-box .filter-items h5,
#vuanem-product-filter .toolbar-sorter.sorter .sorter-desktop .sorter-label{display:none!important}

body.loading-active-12 .list-category-top a{background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),color-stop(72%,#fff),to(#c9d8ff));
    background-image: linear-gradient(180deg,#fff,#fff 72%,#c9d8ff);}
#searchsuite-autocomplete #product li:before{border-bottom:0!important}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .product-type-key-attributes, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .product-type-key-attributes{padding: 15px 0 10px;}
#vuanem-product-filter .block.product-list .arrange-fill .product-description .prod-desc-priceUI-1{margin-bottom:10px;}
 body.category-nem #vuanem-product-filter .block.product-list .arrange-fill:hover {box-shadow:none}
 body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content{bottom:0;      padding-top: 0;}
 body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product{padding-bottom:0}
body:not([class*=category-nem-]) #vuanem-product-filter .block.product-list .arrange-fill , body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill {
    padding: 15px;
    border-left: .5px solid rgba(41,43,183,.2);
    margin: 0;
    transition: all .25s;
    -webkit-transition: all .25s;
    -moz-transition: all .25s;
    -o-transition: all .25s;
    border-bottom: .5px solid rgba(41,43,183,.2);
}
body:not([class*=category-nem-]) #vuanem-product-filter .block.product-list .arrange-fill:nth-child(3n+1) {
    border-left: none;
}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill{padding:0!important;border-bottom:0!important;border-left:0!important}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info{padding-top:10px;padding-bottom:0;}

.catalogsearch-result-index .widget.block.block-static-block .cdz-best-seller-wrap .cdz-block-title{padding-left:15px;}

@media (min-width: 767px){

body #vuanem-header div#searchsuite-autocomplete .product{height:auto!important}
body .product-item .active-tragop .tra-gop-pr{    justify-content: center;}
.main-menu .cdz-main-menu .groupmenu-drop-content.groupmenu-width-24.col-5 .row div.col-sm-4{width:20%!important}
body:not([class*=category-nem-]) #vuanem-product-filter .block.product-list .arrange-fill .item-product, body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill .item-product {
    padding-bottom: 110px;
}
body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product{padding:5px 0px!important;}
.cdz-product-top{height:auto!important}
.products-grid .product-items:not(.owl-carousel)>.product-item .product-item-info {
    padding-bottom: 120px!important;
}
body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .custom-content{display:block!important;max-width:200px}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .list-image-wrapper, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .list-image-wrapper{    margin: 15px 15px 12px 20px;}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .product-title, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .product-title {height:auto!important}
.category-nem #vuanem-product-filter .productprice.listview, [class*=category-nem-] #vuanem-product-filter .productprice.listview {height:auto}

body #header-general {
    min-height: 90px;
}
}
.discount-percent{z-index:2}
.slider-image > div{z-index:1}
.cms-index-index .product-grid{min-height:400px}
body #vuanem-product-filter .block.product-list .load-more a.learn-more{z-index:2}
body #vuanem-product-filter .toolbar-sorter.sorter .sorter-desktop .sorter-options{border-radius:4px;}
#vuanem-product-filter .productprice.listview {
    height: 52px;
}
body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill:last-child {
    border-right: .5px solid rgba(41,43,183,.2);
}

.cms-page-view .page-main .block-static-block .cdz-block-title{margin-bottom:0;    padding-bottom: 30px;}
.cms-page-view .page-wrapper .page-main .widget.block.block-static-block .products-grid .product-items:not(.owl-carousel)>.product-item:nth-child(4n+1){border-left: .5px solid rgba(41,43,183,.2);}
.cms-page-view .page-wrapper .page-main .widget.block.block-static-block .products-grid .product-items:not(.owl-carousel)>.product-item:nth-child(4n) {
    border-right: .5px solid rgba(41,43,183,.2);
}
.cms-page-view .flash-sale-items .product.data.items>.item.content{padding-top:0;}
#vuanem-product-filter .block.filter .filter-options-item  .open:nth-child(3){display:none;}
@media (max-width: 767px){
body #vuanem-header{z-index:9999;}
body .banner-4image .row.mobile{margin-top:15px}
body .section-product .cdz-block-titles h2{font-size:0;}
body #HomeProductOne.section-product .cdz-block-titles h2:after{content:"Ra mắt nệm cao cấp";font-size:20px;}
body #HomeProductTwo.section-product .cdz-block-titles h2:after{content:"BST chăn ga mới nhất";font-size:20px;}
body #HomeProductThree.section-product .cdz-block-titles h2:after{content:"Phụ kiện giấc ngủ";font-size:20px;}
#vuanem-header .main-menu .cdz-horizontal-menu>ul>li.level-top:nth-child(6)>.menu-link:before{display:none}
body #vuanem-header .cdz-main-menu .cdz-horizontal-menu li.level0>a.menu-link {
    border-bottom: none;
    font-size: 20px!important;
}
.list-category-top{padding: 10px 5px 0;}
 .active-search-header.active-keysearch .header-bottom-mobile{position:relative}
.active-keysearch .header-bottom-mobile{position:relative;}

.active-search-header.active-keysearch .search-bottom{display:block}
body .product-listing.grid .item .product .product-link .product-cover {
    padding-bottom: 180px;
}
body .product-item .product .old-price{line-height:18px;}
.product-listing.grid .item .product .left-content-info .price-container{height:87px}
body .store-location .label-store{    white-space: nowrap;}
body .header-bottom-mobile{margin-top:0!important}
body.active-search-header .header-bottom-mobile{position:relative;}
body.cms-tra-gop-online .page-wrapper .page-main .widget.block.block-static-block .products-grid .product-items:not(.owl-carousel)>.product-item {
    padding: 8px 3px!important;
}
.cms-tra-gop-online .page-main{position:relative;overflow:hidden}
.catalog-category-view .category-view {display:none!important}
body.add-padding-header #vuanem-header .header-top{z-index:999}
body .product-item .active-tragop .tra-gop-pr .appen{font-weight:500}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .view-detail a, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .view-detail a{color:#fff!important}
.category-nem #vuanem-product-filter .block.product-list .arrange-fill, [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill{padding:0 10px!important}
 [class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content{width:100%!important}
#vuanem-product-filter .block.filter .filter-options-item  .open:nth-child(3){display:none;}
body .product-item-details .price-box .normal-price{line-height:25px;}
body .left-content-info .price-freeship{padding: 0px 0 5px;}
.price-freeship .discount-percent{    right: 0px;
    margin-right: 5px;}
#vuanem-product-filter .price-box.price-final_price{height:55px;}
body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .view-detail{padding-top:5px;margin-top:0;}
body .block-products-list .products-grid .product-items .product-item .product-item-info .product-item-details .view-detail a{background:#4143DB;}
body .page-wrapper .page-main .widget.block.block-static-block .products-grid .product-items:not(.owl-carousel)>.product-item{    padding: 8px 1px!important;}
#product-detail-main-content .product-detail-first-block .product-detail .old-price {
    margin-left: 0;
}

body .tooltip-free{width:100px;}
.product-detail .price-freeship{display:block}
.product-image-single .icon-play {display:none}
.cms-home .cdz-product-top{height:auto!important}
.banner-landing{    margin: -35px -23px 0!important;    max-width: unset!important;flex: auto!important;}
.banner2 img{margin-bottom:15px;}
.category-nem-nhap-khau-dat-hang-online.catalog-category-view .wrapper-breadcrums .item{display:none!important}
#vuanem-product-filter .toolbar-sorter.sorter .filter-mobile span{display:block!important}
body.active-search-header .header-bottom-mobile .search-bottom {
    height:calc(100vh  - 105px);
}
.loading-active-12  .header-homepage-slider .listing-block .image{height:auto;}
body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info{padding-right:0}
body .vuanem-homepage #vuanem-blog-list{padding:0;}
body.open-filter #vuanem-product-filter{z-index:unset;    z-index: inherit;}
.section-nem-item.mobile.tabs-items-brand{margin-top:10px;}

.loading-active-12 .slideshow-container.not-display-on-mobile{min-height:auto;    min-height: inherit;min-height:unset}
.image-banner.mobile a{position:relative;z-index:1}
body.catalog-product-view #header-general{
    padding-top: 0px!important;
}
body #header-general {
    padding-top: 52px!important;
}

body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill {
  padding: 10px !important;
  border-left: 0.5px solid rgba(41, 43, 183, 0.2);
  margin: 0!important;
  transition: all .25s;
  -webkit-transition: all .25s;
  -moz-transition: all .25s;
  -o-transition: all .25s;
  border-bottom: 0.5px solid rgba(41, 43, 183, 0.2);
}

body #vuanem-product-filter .block.filter .filter-items{height:auto!important}
body #vuanem-product-filter .toolbar-sorter.sorter{padding:10px 0}
body #vuanem-product-filter .block.product-list .filter-list-box{padding-top:10px;}
body #vuanem-product-filter .block.product-list .filter-list-box .filter-items h5{display:none!important}

body #vuanem-product-filter .block.product-list .arrange-fill .productprice .price-box .old-price {
    padding-bottom: 0;
}
body #vuanem-product-filter .block.product-list{margin-top:0;}
 body #vuanem-product-filter .block.product-list .arrange-fill .view-detail{height: auto;border: 0;}
    body #vuanem-product-filter .block.product-list .arrange-fill .view-detail a{display: block;
         
        display: block;
      color: #fff!important;
    background-color: #4143db!important;  border-radius: 4px;
        height: 42px;
        line-height: 42px;
        font-size: 18px;
    }

body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill .tile-content{    padding-top: 5px;}
body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill .product-title{margin-top:5px;      height: 48px;  line-height: 22px;}
body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .productprice.listview {
    height: 55px;
}
body:not([class*=category-nem]) #vuanem-product-filter .block.product-list .arrange-fill{margin-bottom:0}
body #vuanem-flash-sales-new .container .hot-deal .VueCarousel .VueCarousel-wrapper {
    padding: 10px 0;
}
#block-banner-top img{width:100%;}
}
@media (max-width: 374px){
body #vuanem-product-filter .block.product-list .arrange-fill .product-title{margin-bottom:0}
body .price-freeship .discount-percent{    right: 10px;}
}

@media (max-width: 767px) {
.product-detail-main-content .product-image-single .cool-lightbox.cool-lightbox--can-zoom {
    position: fixed !important;
    z-index: 99999999!important;
    background: rgba(30, 30, 30, 0.9) !important;
}
.product-detail-main-content .cool-lightbox div {
    position: absolute !important;
}
.catalog-product-view .block-detail-top .column-left {
z-index: 999;
}
.product-detail-main-content .cool-lightbox div.cool-lightbox-toolbar {
display: block !Important;
}
.product-detail-main-content .cool-lightbox div.cool-lightbox-thumbs {
display: block !Important;
    top: auto !important;
    bottom: 0px;
    z-index: 9999;
}
}
</style></div> <div class="information-website"><div class="items-link login desktop"><i class="icons user"></i> <a href="#" class="content"> <li class="greet welcome" data-bind="scope: 'customer'">
<!-- ko if: customer().fullname  --><!-- /ko -->
<!-- ko ifnot: customer().fullname  -->
<span data-bind="html:&quot;VUA NỆM - Hệ Thống Bán Lẻ Chăn Ra Gối Nệm Lớn Nhất Việt Nam&quot;">VUA NỆM - Hệ Thống Bán Lẻ Chăn Ra Gối Nệm Lớn Nhất Việt Nam</span>
<!-- /ko -->
</li>

<li class="link wishlist" data-bind="scope: 'wishlist'">
<a href="https://vuanem.com/wishlist/">Danh sách yêu thích <!-- ko if: wishlist().counter --><!-- /ko -->
</a>
</li>

<li class="authorization-link switcher log-in" id="authorization-top-link"> <a style="display:none" class="log-in link" href="https://vuanem.com/customer/account/login/referer/aHR0cHM6Ly92dWFuZW0uY29tL2xheW91dA%2C%2C/">
Đăng nhập </a>
<div class="actions dropdown options switcher-options">
<div class="action toggle switcher-trigger" id="authorization-trigger">
Đăng nhập </div>

<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front mage-dropdown-dialog" tabindex="-1" role="dialog" aria-describedby="cdz-login-form-dropdown" style="display: none;"><div style="" id="cdz-login-form-dropdown" class="cdz-login-form-dropdown dropdown switcher-dropdown ui-dialog-content ui-widget-content">
<div class="block block-social-login-mini">
<div class="block-title">
<strong id="block-customer-login-heading" role="heading" aria-level="2">Đăng Nhập Bằng</strong>
</div>
<div class="block-content block-customer-login-mini" aria-labelledby="block-customer-social-login-heading">
<div class="actions-toolbar social-btn">
<a class="btn btn-block btn-social btn-facebook">
<span class="fa fa-facebook"></span>
Facebook </a>
<a class="btn btn-block btn-social btn-google">
<span class="fa fa-google"></span>
Google </a>
</div>
</div>
</div>
<div class="pslogin-spacer pslogin-clearfix">
<table>
<tbody><tr>
<td><div class="pslogin-border"></div></td>
<td class="pslogin-bordertext w25">HOẶC</td>
<td><div class="pslogin-border"></div></td>
</tr>
</tbody>
</table>
</div>
<div class="block block-customer-login">
<div class="block-content">
<form class="form form-login" action="https://vuanem.com/customer/account/loginPost/" method="post" novalidate="novalidate">
<input name="form_key" type="hidden" value="2HLu4GHu25OD5AZK"> <fieldset class="fieldset login" data-hasrequired="* Vui lòng điền thông tin bắt buộc">
<div class="field email required">

<div class="control">
<input placeholder="Email" name="login[username]" value="" autocomplete="off" type="email" class="input-text" title="Email" data-validate="{required:true, 'validate-email':true}" aria-required="true">
</div>
</div>
<div class="field password required">

<div class="control">
<input placeholder="Mật khẩu" name="login[password]" type="password" autocomplete="off" class="input-text" title="Mật khẩu" data-validate="{required:true, 'validate-password':true}" aria-required="true">
</div>
</div>
<div class="actions-toolbar">
<div class="">
<button type="submit" class="action login primary" name="send"><span>Đăng nhập</span></button></div>

<div class="secondary">
<a class="action remind" href="https://vuanem.com/customer/account/forgotpassword/"><span>Bạn quên mật khẩu?</span></a>
</div>
</div>
</fieldset>
</form>
</div>
</div>
</div></div></div>
</li>
<li><a href="https://vuanem.com/customer/account/create/" class="register-link">Tạo tài khoản</a></li></a></div> <div class="information-websites mobile"><div class="items-link phone"><p class="title">Mua hàng, gọi ngay</p> <a href="tel:18002092" class="content">1800 2092</a></div></div> <div class="store-location mobile"><a href="/stores" title="Tìm cửa hàng (showroom) Vua Nệm gần nhất"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMC44MDUiIHZpZXdCb3g9IjAgMCAzMiAzMC44MDUiPgogIDxnIGlkPSJpY29uX2xvY2F0aW9uIiBkYXRhLW5hbWU9Imljb24gbG9jYXRpb24iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMDE4IC01MC4xOTUpIj4KICAgIDxnIGlkPSJFbGxpcHNlXzkxIiBkYXRhLW5hbWU9IkVsbGlwc2UgOTEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMjUgNzEpIiBmaWxsPSIjZmZmIiBzdHJva2U9IiMyOTJiYjciIHN0cm9rZS13aWR0aD0iMSIgb3BhY2l0eT0iMC41Ij4KICAgICAgPGVsbGlwc2UgY3g9IjkiIGN5PSIzLjUiIHJ4PSI5IiByeT0iMy41IiBzdHJva2U9Im5vbmUiLz4KICAgICAgPGVsbGlwc2UgY3g9IjkiIGN5PSIzLjUiIHJ4PSI4LjUiIHJ5PSIzIiBmaWxsPSJub25lIi8+CiAgICA8L2c+CiAgICA8ZyBpZD0iRWxsaXBzZV85MiIgZGF0YS1uYW1lPSJFbGxpcHNlIDkyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMDE4IDY4KSIgZmlsbD0iI2ZmZiIgc3Ryb2tlPSIjMjkyYmI3IiBzdHJva2Utd2lkdGg9IjEiIG9wYWNpdHk9IjAuNSI+CiAgICAgIDxlbGxpcHNlIGN4PSIxNiIgY3k9IjYuNSIgcng9IjE2IiByeT0iNi41IiBzdHJva2U9Im5vbmUiLz4KICAgICAgPGVsbGlwc2UgY3g9IjE2IiBjeT0iNi41IiByeD0iMTUuNSIgcnk9IjYiIGZpbGw9Im5vbmUiLz4KICAgIDwvZz4KICAgIDxwYXRoIGlkPSJQYXRoXzI3IiBkYXRhLW5hbWU9IlBhdGggMjciIGQ9Ik02Ni42MDYsMGE5LjQ1NSw5LjQ1NSwwLDAsMC05LjQ1NSw5LjQ1NWMwLDcuNTcxLDguNTQ3LDE0LjU4Myw4LjU0NywxNC41ODNhMS41LDEuNSwwLDAsMCwxLjgxNiwwczguNTQ3LTcuMDEyLDguNTQ3LTE0LjU4M0E5LjQ1NSw5LjQ1NSwwLDAsMCw2Ni42MDYsMFptMCwxNC4yMTJhNC43NTcsNC43NTcsMCwxLDEsNC43NTctNC43NTdBNC43NjIsNC43NjIsMCwwLDEsNjYuNjA2LDE0LjIxMloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDk2Ny42NDUgNTAuMTk1KSIgZmlsbD0iIzI5MmJiNyIvPgogIDwvZz4KPC9zdmc+Cg==" class="desktop"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1Mi43ODMiIGhlaWdodD0iMzYuNTYxIiB2aWV3Qm94PSIwIDAgNTIuNzgzIDM2LjU2MSI+CiAgPGcgaWQ9ImljLWxvY2F0aW9uIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjIwLjg0NSAtMzIzLjUyNykiPgogICAgPGcgaWQ9Ikdyb3VwXzczNCIgZGF0YS1uYW1lPSJHcm91cCA3MzQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIzMi40OTMgMzQ0LjIwNikiPgogICAgICA8cGF0aCBpZD0iUGF0aF8yNjkiIGRhdGEtbmFtZT0iUGF0aCAyNjkiIGQ9Ik0yNTAuODE1LDM1Ni45OTRjLTcuNDUsMC0xNC45OTItMS40MzItMTQuOTkyLTQuMTY5czcuNTQyLTQuMTcsMTQuOTkyLTQuMTcsMTQuOTkyLDEuNDMzLDE0Ljk5Miw0LjE3UzI1OC4yNjYsMzU2Ljk5NCwyNTAuODE1LDM1Ni45OTRabTAtNy4xNDdhNDMuMjI3LDQzLjIyNywwLDAsMC05Ljk3MiwxLjAyNiwxMS40MzcsMTEuNDM3LDAsMCwwLTIuODkzLDEuMDYyYy0uNTM1LjMxNS0uODI5LjYzMS0uODI5Ljg5LDAsLjUxLDEuMTUxLDEuMzEyLDMuNzIxLDEuOTUyYTQ4Ljk2Miw0OC45NjIsMCwwLDAsMTkuOTQ0LDAsMTEuNDQ4LDExLjQ0OCwwLDAsMCwyLjg5My0xLjA2MmMuNTM1LS4zMTQuODI5LS42MzEuODI5LS44OSwwLS41MS0xLjE1MS0xLjMxMi0zLjcyMi0xLjk1MkE0My4yMjMsNDMuMjIzLDAsMCwwLDI1MC44MTUsMzQ5Ljg0N1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yMzUuODIzIC0zNDguNjU1KSIgZmlsbD0iIzJkMmU3ZiIvPgogICAgPC9nPgogICAgPGcgaWQ9Ikdyb3VwXzczNSIgZGF0YS1uYW1lPSJHcm91cCA3MzUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIyNi40NyAzNDAuNzAyKSIgb3BhY2l0eT0iMC4yIj4KICAgICAgPHBhdGggaWQ9IlBhdGhfMjcwIiBkYXRhLW5hbWU9IlBhdGggMjcwIiBkPSJNMjQ5LjEyMSwzNTkuNzQ1YTQ0LjYsNDQuNiwwLDAsMS0xNC4zNTItMi4xMTJjLTQuMDM5LTEuNDMtNi4yNjMtMy40MDYtNi4yNjMtNS41NjJzMi4yMjUtNC4xMzIsNi4yNjMtNS41NjNhNDkuODQ4LDQ5Ljg0OCwwLDAsMSwyOC43LDBjNC4wMzksMS40MzEsNi4yNjMsMy40MDYsNi4yNjMsNS41NjNzLTIuMjI1LDQuMTMyLTYuMjYzLDUuNTYyQTQ0LjYsNDQuNiwwLDAsMSwyNDkuMTIxLDM1OS43NDVabTAtMTQuMTU3YTQzLjI1LDQzLjI1LDAsMCwwLTEzLjkwNiwyLjAzNWMtMy40NTksMS4yMjUtNS40NDIsMi44NDctNS40NDIsNC40NDhzMS45ODMsMy4yMjMsNS40NDIsNC40NDhhNDguNTQ1LDQ4LjU0NSwwLDAsMCwyNy44MTIsMGMzLjQ1OC0xLjIyNSw1LjQ0Mi0yLjg0Nyw1LjQ0Mi00LjQ0OHMtMS45ODQtMy4yMjMtNS40NDItNC40NDhBNDMuMjU4LDQzLjI1OCwwLDAsMCwyNDkuMTIxLDM0NS41ODhaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjI4LjUwNSAtMzQ0LjM5NykiIGZpbGw9IiMyZDJlN2YiIG9wYWNpdHk9IjAuMjY2Ii8+CiAgICA8L2c+CiAgICA8ZyBpZD0iR3JvdXBfNzM2IiBkYXRhLW5hbWU9Ikdyb3VwIDczNiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjIwLjg0NSAzMzYuNjY1KSIgb3BhY2l0eT0iMC4yIj4KICAgICAgPHBhdGggaWQ9IlBhdGhfMjcxIiBkYXRhLW5hbWU9IlBhdGggMjcxIiBkPSJNMjQ4LjA2MSwzNjQuNzI2YTQ4LjMyNiw0OC4zMjYsMCwwLDEtMTguNDQ4LTMuMjc4Yy01LjEyMy0yLjE5My03Ljk0NC01LjE4OC03Ljk0NC04LjQzNHMyLjgyMi02LjI0Miw3Ljk0NC04LjQzNGE1My41NjIsNTMuNTYyLDAsMCwxLDM2LjksMGM1LjEyMiwyLjE5Miw3Ljk0Myw1LjE4OCw3Ljk0Myw4LjQzNHMtMi44MjIsNi4yNDEtNy45NDMsOC40MzRBNDguMzMzLDQ4LjMzMywwLDAsMSwyNDguMDYxLDM2NC43MjZabTAtMjIuMDU4YTQ3LjE1Nyw0Ny4xNTcsMCwwLDAtMTcuOTkyLDMuMTg0Yy00LjU0NCwxLjk0NS03LjE1LDQuNTU2LTcuMTUsNy4xNjNzMi42MDYsNS4yMTgsNy4xNSw3LjE2M2E1Mi40NDIsNTIuNDQyLDAsMCwwLDM1Ljk4MywwYzQuNTQ0LTEuOTQ1LDcuMTUtNC41NTYsNy4xNS03LjE2M3MtMi42MDYtNS4yMTktNy4xNS03LjE2M0E0Ny4xNTMsNDcuMTUzLDAsMCwwLDI0OC4wNjEsMzQyLjY2OFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yMjEuNjY5IC0zNDEuMzAzKSIgZmlsbD0iIzJkMmU3ZiIgb3BhY2l0eT0iMC4wNiIvPgogICAgPC9nPgogICAgPGcgaWQ9Ikdyb3VwXzczNyIgZGF0YS1uYW1lPSJHcm91cCA3MzciIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIzNy40NzUgMzIzLjUyNykiPgogICAgICA8cGF0aCBpZD0iUGF0aF8yNzIiIGRhdGEtbmFtZT0iUGF0aCAyNzIiIGQ9Ik0yNTAuMzI0LDMyMy41MjdhOS45NDksOS45NDksMCwwLDAtOS45NDksOS45NDljMCw3Ljk2Niw4Ljk5MywxNS4zNDUsOC45OTMsMTUuMzQ1YTEuNTc2LDEuNTc2LDAsMCwwLDEuOTExLDBzOC45OTQtNy4zNzksOC45OTQtMTUuMzQ1QTkuOTQ5LDkuOTQ5LDAsMCwwLDI1MC4zMjQsMzIzLjUyN1ptMCwxNC45NTRhNS4wMDUsNS4wMDUsMCwxLDEsNS4wMDYtNS4wMDVBNS4wMTEsNS4wMTEsMCwwLDEsMjUwLjMyNCwzMzguNDgxWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTI0MC4zNzUgLTMyMy41MjcpIiBmaWxsPSIjMmQyZTdmIi8+CiAgICA8L2c+CiAgICA8Y2lyY2xlIGlkPSJFbGxpcHNlXzIzIiBkYXRhLW5hbWU9IkVsbGlwc2UgMjMiIGN4PSI1LjAzNCIgY3k9IjUuMDM0IiByPSI1LjAzNCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjQyLjQyMSAzMjguNDYyKSIgZmlsbD0iI2ZmZiIvPgogIDwvZz4KPC9zdmc+Cg==" class="mobile"> <div class="label-store">Hệ thống<br>cửa hàng</div></a></div> <div class="mini-cart"><!----> <div class="cart-icon">Giỏ Hàng</div> <!----> <!----></div></div></div></div> <div class="header-center"><div class="container"><div class="content-header"><div class="header-bottom-mobile mobile"><div class="sidebar-mobile"><div class="icons-menu"><div class="line"></div> <div class="line"></div> <div class="line"></div></div></div></div> <div class="logo-header desktop"><a href="/"><img src="https://vuanem.com/media/logo/stores/1/logo-vuanem.png" alt="VUA NỆM - Hệ Thống Bán Lẻ Chăn Ra Gối Nệm Lớn Nhất Việt Nam" width="150" class="main-logo"></a></div> <div class="main-menu desktop"><div class="widget block block-static-block">
<div class="cdz-main-menu cdz-fix-left"><div class="cdz-menu cdz-horizontal-menu   cdz-normal" id="menu-16-6011381838cbf">
<ul class="groupmenu">
<li class="item level0 position-static menu-item-1 level-top parent">
<a class="menu-link" href="/nem?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-nem2.png"></i> <span>Nệm</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu text-content">
<div class="has-submenu groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Chọn kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a title="Mua nệm 1m2 - 2m, đệm 1m2 – 2m" href="/nem?size=28&amp;src=mega-menu">120 x200cm</a></li>
<li class="item"><a title="Mua nệm 1m6 - 2m, đệm 1m26– 2m" href="/nem?size=30&amp;src=mega-menu">160 x200cm</a></li>
<li class="item"><a title="Mua nệm 1m8 – 2m, đệm 1m8 – 2m" href="/nem?size=31&amp;src=mega-menu"><span style="color: #ff0000;">180 x200cm</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Mua nệm 2m - 2m, đệm 2m – 2m" href="/nem?size=32&amp;src=mega-menu">200 x200cm</a></li>
<li class="item"><a title="Mua nệm 2m – 2m2, đệm 2m – 2m2" href="/nem?size=33&amp;src=mega-menu">200 x220cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a title="Mua nệm giá dưới 3 triệu, đệm dưới 3 triệu" href="/nem?price=-3000000&amp;src=mega-menu">Dưới 3 triệu</a></li>
<li class="item"><a title="Mua nệm từ 3 đến 5 triệu, đệm từ 3 đến 5 triệu" href="/nem?price=3000000-5000000&amp;src=mega-menu"><span class="text-sale">3 triệu - 5 triệu</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Mua nệm từ 5 đến 10 triệu, đệm từ 5 đến 10 triệu" href="/nem?price=5000000-10000000&amp;src=mega-menu">5 triệu - 10 triệu</a></li>
<li class="item"><a title="Mua nệm từ 10 đến 30 triệu, đệm từ 10 đến 30 triệu" href="/nem?price=10000000-30000000&amp;src=mega-menu">10 triệu - 30 triệu</a></li>
<li class="item"><a title="Mua nệm từ 30 đến 50 triệu, đệm từ 30 đến 50 triệu" href="/nem?price=30000000-50000000&amp;src=mega-menu">30 triệu - 50 triệu</a></li>
<li class="item"><a title="Mua nệm giá trên 50 triệu, đệm cao cấp" href="/nem?price=50000000-&amp;src=mega-menu">Trên 50 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Loại nệm<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="/nem/bong-ep?src=mega-menu" title="Nệm bông ép, mua nệm bông ép giá rẻ, đệm bông ép">Nệm Bông Ép</a></li>
<li class="item"><a href="/nem/foam?src=mega-menu" title="Nệm foam, mua nệm foam giá rẻ, cao cấp, đệm foam">Nệm Foam</a></li>
<li class="item selling-well"><a href="/nem/cao-su?src=mega-menu" title="Nệm cao su, mua nệm cao su giá rẻ, cao cấp, đệm cao su">Nệm Cao Su</a></li>
<li class="item"><a href="/nem/lo-xo?src=mega-menu" title="Nệm lò xo, mua nệm lò xo giá rẻ, cao cấp, đệm lò xo">Nệm Lò Xo</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Nệm nhập khẩu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a title="Nệm dunlopillo, mua nệm Dunlopillo, đệm dunlopillo" href="/nem?product_brand=298&amp;src=mega-menu"><span class="text-sale">Dunlopillo</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Nệm Therapedic, mua nệm Therapedic, đệm Therapedic" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1125">Therapedic</a></li>
<li class="item"><a title="Nệm Americanstar, mua nệm Americanstar, đệm Americanstar" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1276">Americanstar</a></li>
<li class="item"><a title="Nệm Lady Americana, mua nệm Lady Americana, đệm Lady Americana" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1290">Lady Americana</a></li>
<li class="item"><a title="Nệm Zinus, mua nệm Zinus, đệm Zinus" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1341">Zinus</a></li>
<li class="item"><a title="Nệm Tempur, mua nệm Tempur, đệm Tempur" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=302">Tempur</a></li>
</ul></div>
<div class="col-sm-8"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link banner-brand">
<li class="item hot-item"><span style="color: #ff0000;"><a title="Nệm Amando, mua nệm Amando, đệm amando" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=293"><span style="color: #ff0000;"><img src="https://media.vuanem.com/wysiwyg/brand/logo-amando.jpg" width="88" height="88"></span></a></span></li>
<li class="item"><a title="Nêm Aeroflow, mua nệm Aeroflow, đệm aeroflow" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=300"><img src="https://media.vuanem.com/wysiwyg/brand/logo-aeroflow.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm dunlopillo, mua nệm Dunlopillo, đệm dunlopillo" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=303"><img src="https://media.vuanem.com/wysiwyg/brand/logo-goodnight.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm liên á, mua nệm liên á, đệm liên á" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=299"><img src="https://media.vuanem.com/wysiwyg/brand/logo-lien-a.jpg" width="88" height="88"></a></li>
<li class="item"><a href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1269"><img src="https://media.vuanem.com/wysiwyg/brand/logo-gummi.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm Kim cương, mua nệm Kim cương, đệm Kim cương" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=314"><img src="https://media.vuanem.com/wysiwyg/brand/logo-kimcuong.jpg" width="88" height="88"></a></li>
</ul></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/chan-ga-goi?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-changa.png"></i> <span>Chăn Ga Gối</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Bộ chăn ga<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-chan-ga-chun?src=mega-menu">Bộ chăn ga chun</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-chan-ga-phu?src=mega-menu">Bộ chăn ga phủ</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-ga-chun-va-goi?src=mega-menu">Bộ ga chun và gối</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/chan-le?src=mega-menu">Chăn lẻ</a></li>
<li class="item hot-item"><a href="https://vuanem.com/chan-ga-goi/ga-le?src=mega-menu"><span style="color: #ff0000;"><span style="color: #ff0000;">Ga lẻ</span></span></a></li>
<li class="item"><a title="Mua vỏ gối, bộ chăn ga gối" href="https://vuanem.com/chan-ga-goi/vo-goi?src=mega-menu">Vỏ gối lẻ</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=28">120 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=30">160 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=31">180 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=33">200 x 220cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=321">Free Size</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=1000000-2000000">Từ 1 -2 triệu</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=2000000-3000000">Từ 2 - 3 triệu</a></li>
<li class="item selling-well"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=3000000-4000000">Từ 3 - 4 triệu</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=4000000-9000000">Trên 4 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/brand/canada"><span style="color: #ff0000;"><span style="color: #ff0000;">Canada</span></span></a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=293">Amando</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=306">Dreamland</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=304">Pyeoda</a></li>
</ul></div>
<div class="col-sm-8"><p><img src="https://media.vuanem.com/wysiwyg/homepage/medallion-deluxe-comforter-set-with-sheet-pillowcase-mattress-pad-pillow-grey-bedding-teal-and.jpg" width="710" height="710"></p></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/phu-kien?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-phukien.png"></i> <span>Phụ Kiện</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Phụ kiện giấc ngủ<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a title="Mua gối" href="https://vuanem.com/phu-kien/goi?src=mega-menu">Gối</a></li>
<li class="item"><a title="Gối tựa trang trí" href="https://vuanem.com/phu-kien/goi-tua-trang-tri?src=mega-menu">Gối Tựa Trang Trí</a></li>
<li class="item"><a title="Tấm bảo vệ nệm, đệm" href="https://vuanem.com/phu-kien/tam-bao-ve-nem?src=mega-menu">Tấm Bảo Vệ Nệm</a></li>
<li class="item"><a title="Tấm tăng tiện ích" href="https://vuanem.com/phu-kien/tam-tang-tien-ich-nem?src=mega-menu">Tấm Tăng Tiện Ích Nệm</a></li>
<li class="item"><a title="Ruột chăn, mền" href="https://vuanem.com/phu-kien/ruot-chan?src=mega-menu">Ruột Chăn</a></li>
<li class="item"><a title="Phụ kiện khác" href="https://vuanem.com/phu-kien/phu-kien-khac?src=mega-menu">Phụ kiện khác</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=332">45 x 65cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=334">50 x 70cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=338">60 x 60cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=30">160 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=31">180 x 200cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=-1000000">Dưới 1 triệu</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=1000000-2000000">Từ 1 - 2 triệu</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=3000000-">Trên 3 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=300"><span style="color: #ff0000;"><span style="color: #ff0000;">Aeroflow</span></span></a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=293">Amando</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=312">Arena</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=313">Doona</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=295">Hanvico</a></li>
</ul></div>
<div class="col-sm-8"><p><img src="https://media.vuanem.com/wysiwyg/homepage/medallion-deluxe-comforter-set-with-sheet-pillowcase-mattress-pad-pillow-grey-bedding-teal-and.jpg" width="710" height="710"></p></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/giuong?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-giuong.png"></i> <span>Giường</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Loại giường</p>
<ul class="groupdrop-link size">
<li class="item"><a title="Giường da" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=255"><span class="text-sale">Giường da</span></a></li>
<li class="item hot-item"><a title="Giường Nỉ" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=931">Giường Nỉ</a></li>
<li class="item"><a title="Gối tựa trang trí" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=266">Giường Sắt</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước</p>
<ul class="groupdrop-link size">
<li class="item"><a title="180 x 200 cm" href="#"><span class="text-sale">180 x 200 cm</span><span class="label-hot">Hot</span></a></li>
<li class="item"><a title="200 x 200 cm" href="#">200 x 200 cm</a></li>
<li class="item"><a title="160 x 200 cm" href="#">160 x 200 cm</a></li>
<li class="item"><a title="120 x 200 cm" href="#">120 x 200 cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền</p>
<ul class="groupdrop-link">
<li class="item"><a title="Từ 3 - 5 triệu" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=3000000-5000000">Từ 3 - 5 triệu</a></li>
<li class="item"><a title="Từ 5 - 7 triệu" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=5000000-7000000"><span class="text-sale">Từ 5 - 7 triệu</span><span class="label-hot">Hot</span></a></li>
<li class="item"><a href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=7000000-">Từ 7&nbsp;triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/giuong?src=mega-menu"><span style="color: #ff0000;"><span style="color: #ff0000;">Baya</span></span></a></li>
<li class="item"><a href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_brand=293">Amando</a></li>
</ul></div>
<div class="col-sm-8"></div>
</div>
</div>
</li> </ul>
</li><li class="item level0  level-top">
<a class="menu-link" href="https://vuanem.com/khuyen-mai/xa-soc-hang-trung-bay-tu-999k/"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/blog/Icon/icon-thanhly.png"></i> <span><span style="color:red">Hàng thanh lý</span></span></a>
</li><li class="item level0 mobile color-red level-top">
<a class="menu-link" href="https://vuanem.com/ra-mat-nem-cao-cap-online"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/blog/Icon/ic-khuyen-mai_2x.png"></i> <span>Khuyến mại</span></a>
</li> </ul>
</div>
<nav class="navigation" data-action="navigation">
<ul id="ui-id-1" class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" tabindex="0">
</ul>
</nav></div></div>
</div> <div class="search-bar"><div class="logo-header mobile"><a href="/"><img src="https://vuanem.com/media/logo/stores/1/logo-vuanem.png" alt="VUA NỆM - Hệ Thống Bán Lẻ Chăn Ra Gối Nệm Lớn Nhất Việt Nam" width="60" class="main-logo"></a></div> <div class="search-form-container"><div class="block-icon-seach"><i class="fa fa-search"></i></div> <form id="search_mini_form" action="https://vuanem.com/catalogsearch/result/" method="get" class="form minisearch has-cat"><div class="field search"><div class="control"><input id="search" type="text" name="q" placeholder="Tìm kiếm" maxlength="128" class="input-text"></div> <!----></div> <div class="actions"><button type="submit" title="Tìm kiếm" class="action search primary"><i class="fa fa-search"></i></button></div></form> <!----> <!----></div></div> <div class="store-location desktop"><a href="/stores" title="Tìm cửa hang (showroom) Vua Nệm gần nhất"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMC44MDUiIHZpZXdCb3g9IjAgMCAzMiAzMC44MDUiPgogIDxnIGlkPSJpY29uX2xvY2F0aW9uIiBkYXRhLW5hbWU9Imljb24gbG9jYXRpb24iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMDE4IC01MC4xOTUpIj4KICAgIDxnIGlkPSJFbGxpcHNlXzkxIiBkYXRhLW5hbWU9IkVsbGlwc2UgOTEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMjUgNzEpIiBmaWxsPSIjZmZmIiBzdHJva2U9IiMyOTJiYjciIHN0cm9rZS13aWR0aD0iMSIgb3BhY2l0eT0iMC41Ij4KICAgICAgPGVsbGlwc2UgY3g9IjkiIGN5PSIzLjUiIHJ4PSI5IiByeT0iMy41IiBzdHJva2U9Im5vbmUiLz4KICAgICAgPGVsbGlwc2UgY3g9IjkiIGN5PSIzLjUiIHJ4PSI4LjUiIHJ5PSIzIiBmaWxsPSJub25lIi8+CiAgICA8L2c+CiAgICA8ZyBpZD0iRWxsaXBzZV85MiIgZGF0YS1uYW1lPSJFbGxpcHNlIDkyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMDE4IDY4KSIgZmlsbD0iI2ZmZiIgc3Ryb2tlPSIjMjkyYmI3IiBzdHJva2Utd2lkdGg9IjEiIG9wYWNpdHk9IjAuNSI+CiAgICAgIDxlbGxpcHNlIGN4PSIxNiIgY3k9IjYuNSIgcng9IjE2IiByeT0iNi41IiBzdHJva2U9Im5vbmUiLz4KICAgICAgPGVsbGlwc2UgY3g9IjE2IiBjeT0iNi41IiByeD0iMTUuNSIgcnk9IjYiIGZpbGw9Im5vbmUiLz4KICAgIDwvZz4KICAgIDxwYXRoIGlkPSJQYXRoXzI3IiBkYXRhLW5hbWU9IlBhdGggMjciIGQ9Ik02Ni42MDYsMGE5LjQ1NSw5LjQ1NSwwLDAsMC05LjQ1NSw5LjQ1NWMwLDcuNTcxLDguNTQ3LDE0LjU4Myw4LjU0NywxNC41ODNhMS41LDEuNSwwLDAsMCwxLjgxNiwwczguNTQ3LTcuMDEyLDguNTQ3LTE0LjU4M0E5LjQ1NSw5LjQ1NSwwLDAsMCw2Ni42MDYsMFptMCwxNC4yMTJhNC43NTcsNC43NTcsMCwxLDEsNC43NTctNC43NTdBNC43NjIsNC43NjIsMCwwLDEsNjYuNjA2LDE0LjIxMloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDk2Ny42NDUgNTAuMTk1KSIgZmlsbD0iIzI5MmJiNyIvPgogIDwvZz4KPC9zdmc+Cg=="> <div class="label-store">Tìm cửa hàng<span>quanh đây</span></div></a></div> <div class="information-websites desktop"><div class="items-link phone"><p class="title">Mua hàng, gọi ngay</p> <a href="tel:18002092" class="content">1800 2092</a></div></div></div></div></div> <!----> <div class="header-bottom"><div class="container"><div class="sidebar-mobile"><span>Đóng</span></div> <div class="main-menu"><div class="widget block block-static-block">
<div class="cdz-main-menu cdz-fix-left"><div class="cdz-menu cdz-horizontal-menu   cdz-normal" id="menu-16-6011381838cbf">
<ul class="groupmenu">
<li class="item level0 position-static menu-item-1 level-top parent">
<a class="menu-link" href="/nem?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-nem2.png"></i> <span>Nệm</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu text-content">
<div class="has-submenu groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Chọn kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a title="Mua nệm 1m2 - 2m, đệm 1m2 – 2m" href="/nem?size=28&amp;src=mega-menu">120 x200cm</a></li>
<li class="item"><a title="Mua nệm 1m6 - 2m, đệm 1m26– 2m" href="/nem?size=30&amp;src=mega-menu">160 x200cm</a></li>
<li class="item"><a title="Mua nệm 1m8 – 2m, đệm 1m8 – 2m" href="/nem?size=31&amp;src=mega-menu"><span style="color: #ff0000;">180 x200cm</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Mua nệm 2m - 2m, đệm 2m – 2m" href="/nem?size=32&amp;src=mega-menu">200 x200cm</a></li>
<li class="item"><a title="Mua nệm 2m – 2m2, đệm 2m – 2m2" href="/nem?size=33&amp;src=mega-menu">200 x220cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a title="Mua nệm giá dưới 3 triệu, đệm dưới 3 triệu" href="/nem?price=-3000000&amp;src=mega-menu">Dưới 3 triệu</a></li>
<li class="item"><a title="Mua nệm từ 3 đến 5 triệu, đệm từ 3 đến 5 triệu" href="/nem?price=3000000-5000000&amp;src=mega-menu"><span class="text-sale">3 triệu - 5 triệu</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Mua nệm từ 5 đến 10 triệu, đệm từ 5 đến 10 triệu" href="/nem?price=5000000-10000000&amp;src=mega-menu">5 triệu - 10 triệu</a></li>
<li class="item"><a title="Mua nệm từ 10 đến 30 triệu, đệm từ 10 đến 30 triệu" href="/nem?price=10000000-30000000&amp;src=mega-menu">10 triệu - 30 triệu</a></li>
<li class="item"><a title="Mua nệm từ 30 đến 50 triệu, đệm từ 30 đến 50 triệu" href="/nem?price=30000000-50000000&amp;src=mega-menu">30 triệu - 50 triệu</a></li>
<li class="item"><a title="Mua nệm giá trên 50 triệu, đệm cao cấp" href="/nem?price=50000000-&amp;src=mega-menu">Trên 50 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Loại nệm<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="/nem/bong-ep?src=mega-menu" title="Nệm bông ép, mua nệm bông ép giá rẻ, đệm bông ép">Nệm Bông Ép</a></li>
<li class="item"><a href="/nem/foam?src=mega-menu" title="Nệm foam, mua nệm foam giá rẻ, cao cấp, đệm foam">Nệm Foam</a></li>
<li class="item selling-well"><a href="/nem/cao-su?src=mega-menu" title="Nệm cao su, mua nệm cao su giá rẻ, cao cấp, đệm cao su">Nệm Cao Su</a></li>
<li class="item"><a href="/nem/lo-xo?src=mega-menu" title="Nệm lò xo, mua nệm lò xo giá rẻ, cao cấp, đệm lò xo">Nệm Lò Xo</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Nệm nhập khẩu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a title="Nệm dunlopillo, mua nệm Dunlopillo, đệm dunlopillo" href="/nem?product_brand=298&amp;src=mega-menu"><span class="text-sale">Dunlopillo</span><span class="label-hot">HOT</span></a></li>
<li class="item"><a title="Nệm Therapedic, mua nệm Therapedic, đệm Therapedic" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1125">Therapedic</a></li>
<li class="item"><a title="Nệm Americanstar, mua nệm Americanstar, đệm Americanstar" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1276">Americanstar</a></li>
<li class="item"><a title="Nệm Lady Americana, mua nệm Lady Americana, đệm Lady Americana" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1290">Lady Americana</a></li>
<li class="item"><a title="Nệm Zinus, mua nệm Zinus, đệm Zinus" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1341">Zinus</a></li>
<li class="item"><a title="Nệm Tempur, mua nệm Tempur, đệm Tempur" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=302">Tempur</a></li>
</ul></div>
<div class="col-sm-8"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link banner-brand">
<li class="item hot-item"><span style="color: #ff0000;"><a title="Nệm Amando, mua nệm Amando, đệm amando" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=293"><span style="color: #ff0000;"><img src="https://media.vuanem.com/wysiwyg/brand/logo-amando.jpg" width="88" height="88"></span></a></span></li>
<li class="item"><a title="Nêm Aeroflow, mua nệm Aeroflow, đệm aeroflow" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=300"><img src="https://media.vuanem.com/wysiwyg/brand/logo-aeroflow.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm dunlopillo, mua nệm Dunlopillo, đệm dunlopillo" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=303"><img src="https://media.vuanem.com/wysiwyg/brand/logo-goodnight.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm liên á, mua nệm liên á, đệm liên á" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=299"><img src="https://media.vuanem.com/wysiwyg/brand/logo-lien-a.jpg" width="88" height="88"></a></li>
<li class="item"><a href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=1269"><img src="https://media.vuanem.com/wysiwyg/brand/logo-gummi.jpg" width="88" height="88"></a></li>
<li class="item"><a title="Nệm Kim cương, mua nệm Kim cương, đệm Kim cương" href="https://vuanem.com/nem?brandid=0&amp;catid=3&amp;product_brand=314"><img src="https://media.vuanem.com/wysiwyg/brand/logo-kimcuong.jpg" width="88" height="88"></a></li>
</ul></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/chan-ga-goi?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-changa.png"></i> <span>Chăn Ga Gối</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Bộ chăn ga<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-chan-ga-chun?src=mega-menu">Bộ chăn ga chun</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-chan-ga-phu?src=mega-menu">Bộ chăn ga phủ</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/bo-ga-chun-va-goi?src=mega-menu">Bộ ga chun và gối</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi/chan-le?src=mega-menu">Chăn lẻ</a></li>
<li class="item hot-item"><a href="https://vuanem.com/chan-ga-goi/ga-le?src=mega-menu"><span style="color: #ff0000;"><span style="color: #ff0000;">Ga lẻ</span></span></a></li>
<li class="item"><a title="Mua vỏ gối, bộ chăn ga gối" href="https://vuanem.com/chan-ga-goi/vo-goi?src=mega-menu">Vỏ gối lẻ</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=28">120 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=30">160 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=31">180 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=33">200 x 220cm</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;size=321">Free Size</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=1000000-2000000">Từ 1 -2 triệu</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=2000000-3000000">Từ 2 - 3 triệu</a></li>
<li class="item selling-well"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=3000000-4000000">Từ 3 - 4 triệu</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;price=4000000-9000000">Trên 4 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/brand/canada"><span style="color: #ff0000;"><span style="color: #ff0000;">Canada</span></span></a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=293">Amando</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=306">Dreamland</a></li>
<li class="item"><a href="https://vuanem.com/chan-ga-goi?brandid=0&amp;catid=17&amp;product_brand=304">Pyeoda</a></li>
</ul></div>
<div class="col-sm-8"><p><img src="https://media.vuanem.com/wysiwyg/homepage/medallion-deluxe-comforter-set-with-sheet-pillowcase-mattress-pad-pillow-grey-bedding-teal-and.jpg" width="710" height="710"></p></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/phu-kien?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-phukien.png"></i> <span>Phụ Kiện</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Phụ kiện giấc ngủ<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link size">
<li class="item"><a title="Mua gối" href="https://vuanem.com/phu-kien/goi?src=mega-menu">Gối</a></li>
<li class="item"><a title="Gối tựa trang trí" href="https://vuanem.com/phu-kien/goi-tua-trang-tri?src=mega-menu">Gối Tựa Trang Trí</a></li>
<li class="item"><a title="Tấm bảo vệ nệm, đệm" href="https://vuanem.com/phu-kien/tam-bao-ve-nem?src=mega-menu">Tấm Bảo Vệ Nệm</a></li>
<li class="item"><a title="Tấm tăng tiện ích" href="https://vuanem.com/phu-kien/tam-tang-tien-ich-nem?src=mega-menu">Tấm Tăng Tiện Ích Nệm</a></li>
<li class="item"><a title="Ruột chăn, mền" href="https://vuanem.com/phu-kien/ruot-chan?src=mega-menu">Ruột Chăn</a></li>
<li class="item"><a title="Phụ kiện khác" href="https://vuanem.com/phu-kien/phu-kien-khac?src=mega-menu">Phụ kiện khác</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=332">45 x 65cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=334">50 x 70cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=338">60 x 60cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=30">160 x 200cm</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;size=31">180 x 200cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=-1000000">Dưới 1 triệu</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=1000000-2000000">Từ 1 - 2 triệu</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;price=3000000-">Trên 3 triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=300"><span style="color: #ff0000;"><span style="color: #ff0000;">Aeroflow</span></span></a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=293">Amando</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=312">Arena</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=313">Doona</a></li>
<li class="item"><a href="https://vuanem.com/phu-kien?brandid=0&amp;catid=20&amp;product_brand=295">Hanvico</a></li>
</ul></div>
<div class="col-sm-8"><p><img src="https://media.vuanem.com/wysiwyg/homepage/medallion-deluxe-comforter-set-with-sheet-pillowcase-mattress-pad-pillow-grey-bedding-teal-and.jpg" width="710" height="710"></p></div>
</div>
</div>
</li> </ul>
</li><li class="item level0 position-static level-top parent">
<a class="menu-link" href="/giuong?src=mega-menu"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/v2-new/icon-giuong.png"></i> <span>Giường</span></a>
<ul class="groupmenu-drop slidedown" style="display: none;">
<li class="item level1 has-submenu  text-content">
<div class="has-submenu  groupmenu-drop-content groupmenu-width-24" style=" ">
<div class="row">
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Loại giường</p>
<ul class="groupdrop-link size">
<li class="item"><a title="Giường da" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=255"><span class="text-sale">Giường da</span></a></li>
<li class="item hot-item"><a title="Giường Nỉ" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=931">Giường Nỉ</a></li>
<li class="item"><a title="Gối tựa trang trí" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_material=266">Giường Sắt</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Kích thước</p>
<ul class="groupdrop-link size">
<li class="item"><a title="180 x 200 cm" href="#"><span class="text-sale">180 x 200 cm</span><span class="label-hot">Hot</span></a></li>
<li class="item"><a title="200 x 200 cm" href="#">200 x 200 cm</a></li>
<li class="item"><a title="160 x 200 cm" href="#">160 x 200 cm</a></li>
<li class="item"><a title="120 x 200 cm" href="#">120 x 200 cm</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Giá tiền</p>
<ul class="groupdrop-link">
<li class="item"><a title="Từ 3 - 5 triệu" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=3000000-5000000">Từ 3 - 5 triệu</a></li>
<li class="item"><a title="Từ 5 - 7 triệu" href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=5000000-7000000"><span class="text-sale">Từ 5 - 7 triệu</span><span class="label-hot">Hot</span></a></li>
<li class="item"><a href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;price=7000000-">Từ 7&nbsp;triệu</a></li>
</ul></div>
<div class="col-sm-4"><p class="groupdrop-title font-bold-stag">Thương hiệu<span class="dropdown-icon"><i class="fa fa-angle-right"></i></span></p>
<ul class="groupdrop-link">
<li class="item hot-item"><a href="https://vuanem.com/giuong?src=mega-menu"><span style="color: #ff0000;"><span style="color: #ff0000;">Baya</span></span></a></li>
<li class="item"><a href="https://vuanem.com/giuong?brandid=0&amp;catid=99&amp;product_brand=293">Amando</a></li>
</ul></div>
<div class="col-sm-8"></div>
</div>
</div>
</li> </ul>
</li><li class="item level0  level-top">
<a class="menu-link" href="https://vuanem.com/khuyen-mai/xa-soc-hang-trung-bay-tu-999k/"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/blog/Icon/icon-thanhly.png"></i> <span><span style="color:red">Hàng thanh lý</span></span></a>
</li><li class="item level0 mobile color-red level-top">
<a class="menu-link" href="https://vuanem.com/ra-mat-nem-cao-cap-online"><i class="menu-icon img-icon"><img src="https://vuanem.com/media/wysiwyg/blog/Icon/ic-khuyen-mai_2x.png"></i> <span>Khuyến mại</span></a>
</li> </ul>
</div>
<nav class="navigation" data-action="navigation">
<ul id="ui-id-2" class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" tabindex="0">
</ul>
</nav></div></div>
</div></div></div> <div class="sale-redline"><p style="text-align: center; line-height: 2; margin-bottom: 0px;"><a href="https://vuanem.com/khuyen-mai/chan-ga-amando-mua-xuan/?utm_source=website&amp;utm_medium=red-line&amp;utm_campaign=t12"><span style="color: #ffffff;"><span style="color: #ffffff;"><span style="font-size: medium; font-family: arial, helvetica, sans-serif;">BST chăn ga Amando mùa xuân -</span><span style="text-decoration: underline;"><span style="font-size: medium; font-family: arial, helvetica, sans-serif;"><span style="text-decoration: underline;"> </span></span><span style="font-size: medium; font-family: arial, helvetica, sans-serif;"><span style="text-decoration: underline;">Xem ngay</span></span></span></span></span></a></p></div></div></div></div>