<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 *
 * @package WordPress
 * @subpackage VuaNem
 * @since VuaNem 1.0
 */

?>
		<div id="footer-vuanem"><div class="footer-top-content"><div class="container"><div class="top-footer"><h2 class="title">Tại sao lại chọn Vua Nệm</h2>
<div class="benefit">
<div class="benefit-item">
<div class="image"><a href="https://vuanem.com/khuyen-mai/khao-sat/"> <img src="https://media.vuanem.com/wysiwyg/blog/Icon/footer_top_1.png" width="101" height="70"></a></div>
<p>60 giây<br><span> chọn nệm</span></p>
<p class="detail">Cùng Vua Nệm đi tìm chiếc nệm phù hợp nhất với bạn và gia đình chỉ trong vài bước duy nhất</p>
</div>
<div class="benefit-item">
<div class="image free-trial-image"><a href="https://vuanem.com/chinh-sach-doi-tra-san-pham"><img src="https://media.vuanem.com/wysiwyg/blog/Icon/footer_top_2.png" width="101" height="70"></a></div>
<p class="free-trial">365 đêm<br><span> nằm thử</span></p>
<p class="detail">Hãy “làm quen” với người bạn đồng hành trong hành trình đi tìm giấc mơ đẹp nào</p>
</div>
<div class="benefit-item">
<div class="image"><a href="https://vuanem.com/tra-gop"><img src="https://media.vuanem.com/wysiwyg/blog/Icon/footer_top_31.png" width="101" height="70"></a></div>
<p>Trả góp<br><span> 0% lãi suất</span></p>
<p class="detail">Ngủ ngon mà vẫn dày ví, xua tan nỗi lo tài chính khi sử dụng những sản phẩm cao cấp, chất lượng</p>
</div>
<div class="benefit-item">
<div class="image"><a href="https://vuanem.com/chinh-sach-van-chuyen-giao-nhan"><img src="https://media.vuanem.com/wysiwyg/blog/Icon/footer_top_4.png" width="101" height="70"></a></div>
<p>Vận chuyển<br><span> miễn phí</span></p>
<p class="detail">Giao hàng tận giường, miễn phí các đơn hàng từ 1.000.000đ trở lên đối với tất cả sản phẩm</p>
</div>
</div>
<style xml="space">
#footer-vuanem .benefit-item .image img{
  margin: 0 auto;
}
</style>
<style xml="space">
 body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .custom-content, body[class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .custom-content{display:none}
body.category-nem #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info, body[class*=category-nem-] #vuanem-product-filter .block.product-list .arrange-fill .item-product .tile-content .tile-primary .left-content-info{max-width:100%}

</style></div></div></div> <div class="container"><div class="bottom-footer"><div class="bottom-footer-container"><div class="bottom-footer-item report">
<div class="image"><a href="/"><img src="https://vuanem.com/static/version1608281598/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/vuanem-logo.png" alt="vuanem-logo"></a></div>
<p>Gọi mua hàng (8h30 - 21:00)<br><span><a href="tel:18002092">1800 2092</a></span></p>
<p>Gọi khiếu nại (8h00 - 17h30)<br><span><a href="tel:18002093">1800 2093</a></span></p>
<p>E-mail<br><span>online@vuanem.com</span></p>
<p>Nghỉ chiều thứ 7 và Chủ nhật</p>
</div>
<div class="bottom-footer-item">
<p class="title-bottom"><span>Thông tin công ty</span></p>
<p><a href="/gioi-thieu">Giới thiệu công ty</a></p>
<p><a href="/contact">Liên hệ</a></p>
<p><a title="Tìm cửa hang (showroom) Vua Nệm gần nhất" href="/stores">Xem hệ thống cửa hàng</a></p>
<p><a href="/doi-tac">Đối tác của Vua Nệm</a></p>
<p class="social"><span>Social</span></p>
<div class="social-symbol">
<div class="fb-symbol"><a href="https://www.facebook.com/vuanem.vn/" rel="noopener"><i class="fa fa-facebook"></i></a></div>
<div class="symbol"><a href="https://www.youtube.com/channel/UC6atxoNoxsa2BYKE4_hqwLA" rel="noopener"><i class="fa fa-youtube"></i></a></div>
<div class="symbol"><a href="https://www.instagram.com/vuanem/" rel="noopener"><i class="fa fa-instagram"></i></a></div>
</div>
</div>
<div class="bottom-footer-item">
<p class="title-bottom"><span>Tin tức</span></p>
<p><a href="/blog/khuyen-mai">Khuyến mại</a></p>
<p><a href="/blog/suc-khoe-giac-ngu">Sức Khoẻ Giấc Ngủ</a></p>
<p><a href="/blog/chuyen-gia-nem">Chuyên Gia Nệm</a></p>
<p><a href="https://vieclam.vuanem.com/">Tuyển dụng</a></p>
</div>
<div class="bottom-footer-item">
<p class="title-bottom"><span>Hỗ trợ</span></p>
<p><a href="/dieu-khoan-dieu-kien">Điều khoản &amp; Điều kiện</a></p>
<p><a href="/chinh-sach-bao-mat">Chính sách bảo mật</a></p>
<p><a href="/chinh-sach-bao-hanh">Chính sách bảo hành</a></p>
<p><a href="/phuong-thuc-thanh-toan">Phương thức thanh toán</a></p>
<p><a title="Miễn phí vận chuyển giao nhận nệm" href="/chinh-sach-van-chuyen-giao-nhan">Chính sách vận chuyển &amp; giao nhận</a></p>
<p><a href="/dieu-khoan-mua-ban-hang-hoa">Điều khoản Mua Bán Hàng Hoá</a></p>
<p><a title="100 đêm nằm thử miễn phí tại Vua Nệm" href="/chinh-sach-doi-tra-san-pham">Chính sách đổi trả sản phẩm</a></p>
<p><a href="/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></p>
</div></div> <div class="benefit"><div class="moit"><a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=46342" rel="noopener"><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/bocongthuong.svg" alt="bo-cong-thuong-logo"></a></div> <div class="copy-right"><p>Mã số doanh nghiệp 0107968516 do Sở Kế hoạch Đầu tư Hà Nội cấp lần 1 ngày 18/8/2017</p> <div class="payment-method"><div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/icon-COD.svg" alt="COD-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/visa.svg" alt="visa-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/master-card.svg" alt="mastercard-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/america-express.svg" alt="america-express-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/JCB.svg" alt="JCB-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/onepay.svg" alt="onepay-logo"></div> <div><img src="https://vuanem.com/static/version1610533911/frontend/Codazon/fastest_furniture/vi_VN/VuaNem_Webpack/css/assets/footer/payment/VN-pay.svg" alt="VNPay-logo"></div></div></div></div></div></div></div>